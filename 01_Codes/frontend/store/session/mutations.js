export default {
  getSessionToken(state) {
    return state.sessionToken;
  },

  setSessionToken(state, sessionToken = null) {
    state.sessionToken = sessionToken;
  },
};
