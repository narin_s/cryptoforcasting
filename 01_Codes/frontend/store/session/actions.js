export default {
  getSessionToken({ commit, state }, sessionToken) {
    commit("setSessionToken", sessionToken);
    return state.sessionToken;
  },

  setSessionToken({ commit, state }) {
    this.$axios
      .get("/GetToken", {
        headers: { accessID: process.env.accessId },
      })
      .then((res) => {
        if (res.status === 200) {
          commit("setSessionToken", res.data.sessionToken);
        }
      })
      .catch((error) => {
        //console.log("error :", error);
      });
    return state.sessionToken;
  },
};
