export default {
  setLineCustomerSelectedSubject({ commit, state }, language) {
    commit('setCurrentLanguage', language)
    return state.currentLanguage
  },

  getLineCustomerSelectedSubject({ state }) {
    return state.currentLanguage
  },
}
