import Vue from "vue";
import moment from "moment";

moment.locale("th");
Vue.filter("dateFormat", (value, format) => {
  if (!format) {
    format = "YYYY-MM-DD";
  }

  return value ? moment(value).format(format) : null;
});

Vue.filter("dateTimeFormat", (value, format) => {
  if (!format) {
    format = "YYYY-MM-DD HH:mm:ss";
  }

  return value ? moment(value).format(format) : null;
});

Vue.filter("dateForHumans", (value) => {
  return moment(value).fromNow();
});

Vue.filter("dateFormatForHumans", (value) => {
  if (!value) {
    return "";
  }

  return moment(value).add(543, "year").format("D MMM YYYY");
});

Vue.filter("dateFormatForHumans_EN", (value) => {
  if (!value) {
    return "";
  }

  return moment(value).locale("en").format("D MMM YYYY");
});

Vue.filter("dateTimeFormatForHumans", (value) => {
  if (!value) {
    return "";
  }

  return moment(value).format("D MMM YYYY HH:mm") + "น.";
});
