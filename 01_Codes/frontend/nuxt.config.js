import colors from "vuetify/es5/util/colors";
import { config as dotEnvConfig } from "dotenv";

dotEnvConfig();
export default {
  loading: "~/components/LoadingBar.vue",
  loadingIndicator: {
    name: "circle",
    color: "#3B8070",
    background: "white",
  },
  // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
  ssr: false,

  server: {
    host: "0.0.0.0", // default: localhost,
    port: process.env.PORT || 9999,
  },

  auth: {
    strategies: {
      local: {
        endpoints: {
          login: {
            url: "/Authen/Login",
            method: "post",
            propertyName: "Token",
          },
          user: {
            url: "/Authen/GetLoginProfile",
            method: "post",
            propertyName: "ProfileData",
          },
          logout: {
            url: "/Authen/Logout",
            method: "post",
          },
        },
        tokenName: "Authorization",
      },
    },
    redirect: {
      login: "/login",
    },
  },

  env: {
    accessId: process.env.ACCESS_ID,
    apiUrl: process.env.API_URL,
    apiFileUrl: process.env.API_FILE_URL,
  },
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: "%s - crypto-app",
    title: "crypto-app",
    htmlAttrs: {
      lang: "en",
    },
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "" },
      { name: "format-detection", content: "telephone=no" },
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: ["~/plugins/axios", "~/plugins/filters"],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/vuetify
    "@nuxtjs/vuetify",
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    [
      "@nuxtjs/axios",
      {
        baseURL: process.env.API_URL,
      },
    ],
    "@nuxtjs/auth",
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  // axios: {},
  axios: {
    //baseURL: 'http://localhost:12345/api'
  },

  // proxy: {
  //   '/api/': { target: process.env.API_URL, pathRewrite: {'^/api/': ''}, changeOrigin: true }
  // },
  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ["~/assets/variables.scss"],
    theme: {
      dark: true,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3,
        },
      },
    },
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},
};
