# Discovered Components

This is an auto-generated list of components discovered by [nuxt/components](https://github.com/nuxt/components).

You can directly use them in pages and other components without the need to import them.

**Tip:** If a component is conditionally rendered with `v-if` and is big, it is better to use `Lazy` or `lazy-` prefix to lazy load.

- `<AttachmentDialog>` | `<attachment-dialog>` (components/AttachmentDialog.vue)
- `<AttachmentOtherDialog>` | `<attachment-other-dialog>` (components/AttachmentOtherDialog.vue)
- `<BottomNavigation>` | `<bottom-navigation>` (components/BottomNavigation.vue)
- `<ChangeLanguageDialog>` | `<change-language-dialog>` (components/ChangeLanguageDialog.vue)
- `<ChooseTransferNetworkDialog>` | `<choose-transfer-network-dialog>` (components/ChooseTransferNetworkDialog.vue)
- `<ChooseWebTradeDialog>` | `<choose-web-trade-dialog>` (components/ChooseWebTradeDialog.vue)
- `<ConfirmLogoutDialog>` | `<confirm-logout-dialog>` (components/ConfirmLogoutDialog.vue)
- `<ErrorDialog>` | `<error-dialog>` (components/ErrorDialog.vue)
- `<HelloWorld>` | `<hello-world>` (components/HelloWorld.vue)
- `<LoadingBar>` | `<loading-bar>` (components/LoadingBar.vue)
- `<NuxtLogo>` | `<nuxt-logo>` (components/NuxtLogo.vue)
- `<OTPDialog>` | `<o-t-p-dialog>` (components/OTPDialog.vue)
- `<QRCode>` | `<q-r-code>` (components/QRCode.vue)
- `<RegisterDialog>` | `<register-dialog>` (components/RegisterDialog.vue)
- `<ServicePriceTable>` | `<service-price-table>` (components/ServicePriceTable.vue)
- `<SuccessDialog>` | `<success-dialog>` (components/SuccessDialog.vue)
- `<ToggleChangeLanguage>` | `<toggle-change-language>` (components/ToggleChangeLanguage.vue)
- `<Tutorial>` | `<tutorial>` (components/Tutorial.vue)
- `<VuetifyLogo>` | `<vuetify-logo>` (components/VuetifyLogo.vue)
