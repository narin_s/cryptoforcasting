export { default as AttachmentDialog } from '../../components/AttachmentDialog.vue'
export { default as AttachmentOtherDialog } from '../../components/AttachmentOtherDialog.vue'
export { default as BottomNavigation } from '../../components/BottomNavigation.vue'
export { default as ChangeLanguageDialog } from '../../components/ChangeLanguageDialog.vue'
export { default as ChooseTransferNetworkDialog } from '../../components/ChooseTransferNetworkDialog.vue'
export { default as ChooseWebTradeDialog } from '../../components/ChooseWebTradeDialog.vue'
export { default as ConfirmLogoutDialog } from '../../components/ConfirmLogoutDialog.vue'
export { default as ErrorDialog } from '../../components/ErrorDialog.vue'
export { default as HelloWorld } from '../../components/HelloWorld.vue'
export { default as LoadingBar } from '../../components/LoadingBar.vue'
export { default as NuxtLogo } from '../../components/NuxtLogo.vue'
export { default as OTPDialog } from '../../components/OTPDialog.vue'
export { default as QRCode } from '../../components/QRCode.vue'
export { default as RegisterDialog } from '../../components/RegisterDialog.vue'
export { default as ServicePriceTable } from '../../components/ServicePriceTable.vue'
export { default as SuccessDialog } from '../../components/SuccessDialog.vue'
export { default as ToggleChangeLanguage } from '../../components/ToggleChangeLanguage.vue'
export { default as Tutorial } from '../../components/Tutorial.vue'
export { default as VuetifyLogo } from '../../components/VuetifyLogo.vue'

// nuxt/nuxt.js#8607
function wrapFunctional(options) {
  if (!options || !options.functional) {
    return options
  }

  const propKeys = Array.isArray(options.props) ? options.props : Object.keys(options.props || {})

  return {
    render(h) {
      const attrs = {}
      const props = {}

      for (const key in this.$attrs) {
        if (propKeys.includes(key)) {
          props[key] = this.$attrs[key]
        } else {
          attrs[key] = this.$attrs[key]
        }
      }

      return h(options, {
        on: this.$listeners,
        attrs,
        props,
        scopedSlots: this.$scopedSlots,
      }, this.$slots.default)
    }
  }
}
