import Vue from 'vue'
import Router from 'vue-router'
import { normalizeURL, decode } from 'ufo'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _22a9eb6d = () => interopDefault(import('../pages/forecast/index.vue' /* webpackChunkName: "pages/forecast/index" */))
const _25cc4fdf = () => interopDefault(import('../pages/forget-password/index.vue' /* webpackChunkName: "pages/forget-password/index" */))
const _a604678a = () => interopDefault(import('../pages/inspire.vue' /* webpackChunkName: "pages/inspire" */))
const _7430c78a = () => interopDefault(import('../pages/login.vue' /* webpackChunkName: "pages/login" */))
const _0dacb2aa = () => interopDefault(import('../pages/payment/index.vue' /* webpackChunkName: "pages/payment/index" */))
const _49fc1a8d = () => interopDefault(import('../pages/profile/index.vue' /* webpackChunkName: "pages/profile/index" */))
const _251c9e15 = () => interopDefault(import('../pages/register/index.vue' /* webpackChunkName: "pages/register/index" */))
const _58fbffea = () => interopDefault(import('../pages/reset-password/index.vue' /* webpackChunkName: "pages/reset-password/index" */))
const _e851cd9a = () => interopDefault(import('../pages/support/index.vue' /* webpackChunkName: "pages/support/index" */))
const _aaedb71a = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))

const emptyFn = () => {}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: '/',
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/forecast",
    component: _22a9eb6d,
    name: "forecast"
  }, {
    path: "/forget-password",
    component: _25cc4fdf,
    name: "forget-password"
  }, {
    path: "/inspire",
    component: _a604678a,
    name: "inspire"
  }, {
    path: "/login",
    component: _7430c78a,
    name: "login"
  }, {
    path: "/payment",
    component: _0dacb2aa,
    name: "payment"
  }, {
    path: "/profile",
    component: _49fc1a8d,
    name: "profile"
  }, {
    path: "/register",
    component: _251c9e15,
    name: "register"
  }, {
    path: "/reset-password",
    component: _58fbffea,
    name: "reset-password"
  }, {
    path: "/support",
    component: _e851cd9a,
    name: "support"
  }, {
    path: "/",
    component: _aaedb71a,
    name: "index"
  }],

  fallback: false
}

export function createRouter (ssrContext, config) {
  const base = (config._app && config._app.basePath) || routerOptions.base
  const router = new Router({ ...routerOptions, base  })

  // TODO: remove in Nuxt 3
  const originalPush = router.push
  router.push = function push (location, onComplete = emptyFn, onAbort) {
    return originalPush.call(this, location, onComplete, onAbort)
  }

  const resolve = router.resolve.bind(router)
  router.resolve = (to, current, append) => {
    if (typeof to === 'string') {
      to = normalizeURL(to)
    }
    return resolve(to, current, append)
  }

  return router
}
