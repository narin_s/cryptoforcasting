<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use phpDocumentor\Reflection\Types\Nullable;

class CreateCryptoCurrenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('CryptoCurrency', function (Blueprint $table) {
            $table->bigIncrements('Id')->unsigned();
            $table->string('Symbol',50);
            $table->string('Name', 255);
            $table->string('SymbolUnitConvert',50);
            
            $table->timestamp('CreatedDate')->nullable();
            $table->timestamp('UpdatedDate')->nullable();
            $table->softDeletes('DeletedDate')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('CryptoCurrency');
    }
}
