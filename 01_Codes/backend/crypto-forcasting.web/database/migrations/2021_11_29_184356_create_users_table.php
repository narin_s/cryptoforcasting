<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('User', function (Blueprint $table) {
            $table->bigIncrements('Id')->unsigned();
            $table->string('ProfileName', 255);
            $table->string('Email', 500);
            $table->string('Login', 500);
            $table->string('Password', 500);
            $table->string('PrivateKey', 100);
            $table->boolean('IsActive')->default(false);
            $table->dateTime('ExpiredDate')->nullable();
            
            $table->timestamp('CreatedDate')->nullable();
            $table->timestamp('UpdatedDate')->nullable();
            $table->softDeletes('DeletedDate')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('User');
    }
}
