<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRegistersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Register', function (Blueprint $table) {
            $table->bigIncrements('Id')->unsigned();
            $table->string('RegisterNo', 50);
            $table->string('ProfileName', 255);
            $table->unsignedBigInteger('UserId');
            $table->string('Login', 500);
            $table->string('SecretKey', 500);
            $table->string('Email', 500);
            $table->tinyInteger('Status')->unsigned();
            $table->integer('PeroidSnapShot')->unsigned();
            $table->tinyInteger('UnitPeroidSnapShot')->unsigned();
            
            $table->timestamp('CreatedDate')->nullable();
            $table->timestamp('UpdatedDate')->nullable();
            $table->softDeletes('DeletedDate')->nullable();
        });

        Schema::enableForeignKeyConstraints();
        Schema::table('Register', function (Blueprint $table) {
            $table->foreign('UserId', 'FK_UserId_User_Id')
                    ->references('Id')->on('User');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Register');
    }
}
