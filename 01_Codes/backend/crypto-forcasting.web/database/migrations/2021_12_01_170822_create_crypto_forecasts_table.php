<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCryptoForecastsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('CryptoForecast', function (Blueprint $table) {
            $table->unsignedBigInteger('Id')->primary();
            $table->unsignedBigInteger('CryptoId')->nullable();
            $table->string('SymbolSnapShot', 50);
            $table->string('SymbolUnitConvSnapShot', 50);
            $table->boolean('IsPublish')->default(false);
            
            $table->timestamp('CreatedDate')->nullable();
            $table->timestamp('UpdatedDate')->nullable();
            $table->softDeletes('DeletedDate')->nullable();
        });

        Schema::enableForeignKeyConstraints();
        Schema::table('CryptoForecast', function (Blueprint $table) {
            $table->foreign('CryptoId', 'FK_CryptoId_CryptoCurrency_Id')
                    ->references('Id')->on('CryptoCurrency')->nullOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('CryptoForecast');
    }
}
