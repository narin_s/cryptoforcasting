<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Payment', function (Blueprint $table) {
            $table->unsignedBigInteger('Id')->primary();
            $table->tinyInteger('Type')->unsigned();
            $table->unsignedBigInteger('ReferenceId');
            $table->decimal('SetPriceSnapShot', 18, 6);
            $table->tinyInteger('UnitCodeSnapShot')->unsigned();
            $table->string('SymbolSnapShot', 50);
            $table->decimal('ExchangeRate', 24, 14);
            $table->decimal('TotalPaid', 24, 14);
            $table->string('OrderType', 10);
            $table->string('TransactionId', 255);
            $table->string('TransactionDate',26);
            $table->decimal('ActualPaid', 24, 14);
            $table->string('Currency', 50);

            $table->timestamp('CreatedDate')->nullable();
            $table->timestamp('UpdatedDate')->nullable();
            $table->softDeletes('DeletedDate')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Payment');
    }
}
