<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCryptoForecastItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('CryptoForecastItem', function (Blueprint $table) {
            $table->unsignedBigInteger('Id')->primary();
            $table->unsignedBigInteger('CryptoForecastId');
            $table->tinyInteger('Type')->unsigned();
            $table->tinyInteger('SuggestStatus');
            $table->date('ForcastDate');
            
            $table->timestamp('CreatedDate')->nullable();
            $table->timestamp('UpdatedDate')->nullable();
            $table->softDeletes('DeletedDate')->nullable();
        });

        Schema::enableForeignKeyConstraints();
        Schema::table('CryptoForecastItem', function (Blueprint $table) {
            $table->foreign('CryptoForecastId', 'FK_CryptoForecastId_CryptoForecast_Id')
                    ->references('Id')->on('CryptoForecast');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('CryptoForecastItem');
    }
}
