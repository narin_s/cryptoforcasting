<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePackagePricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('PackagePrice', function (Blueprint $table) {
            $table->bigIncrements('Id')->unsigned();
            $table->integer('SeqNo')->unsigned();
            $table->string('NameTH', 500);
            $table->string('NameEN', 500)->nullable();
            $table->integer('Peroid')->unsigned();
            $table->tinyInteger('UnitPeroid')->unsigned();
            $table->decimal('SetPrice', 18, 6);
            $table->tinyInteger('UnitCode')->unsigned();
            $table->boolean('IsPublish')->default(false);
            
            $table->timestamp('CreatedDate')->nullable();
            $table->timestamp('UpdatedDate')->nullable();
            $table->softDeletes('DeletedDate')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('PackagePrice');
    }
}
