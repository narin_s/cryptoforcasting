@component('mail::message')
# OTP Verification

<b>Dear : {{$user->ProfileName}}</b>

Confirm For Reset Password <b>[refKey: {{$refKey}}]</b> ,<br>
Here is your account confirm OTP in {{$lifetime}} minutes:
<b>{{$otp}}</b>

Thanks,<br>
{{ config('app.name') }}
@endcomponent
