@component('mail::message')
# Account Verification

<b>Dear : {{$user->ProfileName}}</b>

Confirm For Reset Password <b>[refKey: {{$refKey}}]</b> ,<br>
Here is your account confirm link in {{$lifetime}} minutes:
@component('mail::button', ['url' => $url])
Confirm
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
