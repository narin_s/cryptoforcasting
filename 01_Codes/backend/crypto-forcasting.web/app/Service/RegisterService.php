<?php

namespace App\Service;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use App\Constants\Common;
use App\Constants\UnitType;
use App\Constants\PaymentType;
use App\Constants\RegisterStatus;
use App\Models\Register;
use App\Models\User;
use App\Models\Payment;
use App\Http\Resources\RegisterCollection;
use Exception;

class RegisterService
{

    public function saveRegisterToCache($request, $sessionLinkTime)
    {
        $regKey = uniqid();
        $expiredTime =  now(7)->addMinute($sessionLinkTime);
        Cache::store('register')->add($regKey, $request, $expiredTime);
        Cache::store('dupRequest')->add($request["Login"], $request["Email"], $expiredTime);

        return $regKey;
    }

    public function getpaymentMethod()
    {
        $paymentQR = env('BINANCE_PAYMENT_QR');
        $paymentID = env('BINANCE_PAYMENT_ID');
        return  array(
            "QRPaymentURL" => Storage::url($paymentQR),
            "PaymentID" => $paymentID,
        );
    }

    public function registerMember($request, $paymentData, $packagePrice, $cryptoCurrency)
    {
        #region Prepare
        $exchangeRate =  $packagePrice->SetPrice > 0
            ? $packagePrice->SetPrice / $request['CryptoSetPrice']
            : 1;
        $regStatus = RegisterStatus::Commit;
        $currentDate = now(7);
        $expiredDate = $this->getExpiredDate($currentDate, $packagePrice);
        $profileName = $request['ProfileName'];
        $login = trim($request['Login'], " ");
        $email = trim($request['Email'], " ");
        $password = Hash::make($request['Password']);
        #endregion
        try {
            DB::beginTransaction();
            $userId = User::insertGetId([
                'ProfileName' => $profileName,
                'Login' => $login,
                'Password' => $password,
                'Email' => $email,
                'PrivateKey' => uniqid(),
                'IsActive' => true,
                'ExpiredDate' => $expiredDate,
                'CreatedDate' => $currentDate,
            ]);
            $registerId = Register::insertGetId([
                'RegisterNo' => Str::random(20),
                'ProfileName' => $profileName,
                'UserId' => $userId,
                'Login' => $login,
                'SecretKey' => $password,
                'Email' => $email,
                'Status' => $regStatus,
                'PeroidSnapShot' => $packagePrice->Peroid,
                'UnitPeroidSnapShot' => $packagePrice->UnitPeroid,
                'CreatedDate' => $currentDate,
            ]);

            $this->savePayment(
                array(
                    'PaymentType' => PaymentType::Register,
                    'RefId'  => $registerId,
                    'SetPrice' => $packagePrice->SetPrice,
                    'UnitCode' => $packagePrice->UnitCode,
                    'Symbol' => $cryptoCurrency->Symbol,
                    'ExchangeRate' => $exchangeRate,
                    'CryptoSetPrice' => $request['CryptoSetPrice'],
                    'OrderType' => $paymentData['orderType'],
                    'TransactionId' => $paymentData['transactionId'],
                    'TransactionTime' => $paymentData['transactionTime'],
                    'Amount' => $paymentData['amount'],
                    'Currency' => $paymentData['currency'],
                )
            );

            DB::commit();
            return $registerId;
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    public function getExpiredDate($currentDate, $packagePrice)
    {
        $expiredDate = $currentDate;
        if ($packagePrice->UnitPeroid === UnitType::Day) {
            $expiredDate->addDays($packagePrice->Peroid);
        } else if ($packagePrice->UnitPeroid === UnitType::Week) {
            $expiredDate->addDays($packagePrice->Peroid * 7);
        } else if ($packagePrice->UnitPeroid === UnitType::Month) {
            $expiredDate->addMonths($packagePrice->Peroid);
        } else if ($packagePrice->UnitPeroid === UnitType::Year) {
            $expiredDate->addYears($packagePrice->Peroid);
        }
        return $expiredDate;
    }

    public function savePayment($paymentModel)
    {
        $payment = new Payment();
        $payment->Type = $paymentModel['PaymentType'];
        $payment->ReferenceId = $paymentModel['RefId'];
        $payment->SetPriceSnapShot = $paymentModel['SetPrice'];
        $payment->UnitCodeSnapShot = $paymentModel['UnitCode'];
        $payment->SymbolSnapShot = $paymentModel['Symbol'];
        $payment->ExchangeRate = $paymentModel['ExchangeRate'];
        $payment->TotalPaid = $paymentModel['CryptoSetPrice'];
        $payment->OrderType = $paymentModel['OrderType'];
        $payment->TransactionId = $paymentModel['TransactionId'];
        $payment->TransactionDate = $paymentModel['TransactionTime'];
        $payment->ActualPaid = $paymentModel['Amount'];
        $payment->Currency = $paymentModel['Currency'];
        $payment->save();
    }

    public function validateMemberAccount($request)
    {
        $resultEN = array();
        $resultTH = array();
        $profileName = trim($request['ProfileName'], " ");
        $login = trim($request['Login'], " ");
        $email = trim($request['Email'], " ");
        $password = trim($request['Password'], " ");

        if (Common::IsNullOrEmptyString($profileName)) {
            array_push($resultEN, 'Profile Name is required.');
            array_push($resultTH, 'โปรดระบุชื่อโปรไฟล์');
        }

        if (Common::IsNullOrEmptyString($login)) {
            array_push($resultEN, 'Login is required.');
            array_push($resultTH, 'โปรดระบุล็อกอิน');
        } else if (strlen($login) < 5) {
            array_push($resultEN, 'Login less than 5 character.');
            array_push($resultTH, 'ระบุล็อกอินอย่างน้อย 5 ตัวอักษร');
        }

        if (Common::IsNullOrEmptyString($password)) {
            array_push($resultEN, 'Password is required.');
            array_push($resultTH, 'โปรดระบุรหัสผ่าน');
        } else if (strlen($password) < 8) {
            array_push($resultEN, 'Password less than 8 character.');
            array_push($resultTH, 'ระบุรหัสผ่านอย่างน้อย 8 ตัวอักษร');
        }

        if (Common::IsNullOrEmptyString($email)) {
            array_push($resultEN, 'Email is required.');
            array_push($resultTH, 'โปรดระบุอีเมล');
        }

        $existEmailUser = User::where('IsActive', '=', true)
            ->where('Login', $login)
            ->Where('Email', $email)
            ->limit(1)->get();
        if ($existEmailUser !== null && count($existEmailUser) > 0) {
            $existMsg = ($login === $email
                ? 'Email[' . $email . '] has been duplicate.' .
                '||'  . 'อีเมล [' . $email . '] ใช้งานอยู่แล้วในระบบ'
                : 'Cannot allow to use [' . $login . '] as login and [' . $email . '] as email.' .
                '||' . 'ไม่อนุญาตให้ใช้งานล็อกอิน [' . $login . '] และอีเมล [' . $email . ']');
            $existMsg =  explode('||', $existMsg);
            array_push($resultEN, $existMsg[0]);
            array_push($resultTH, $existMsg[1]);
        }

        if (count($resultEN) > 0 || count($resultTH) > 0)
            return [
                'ErrorMessageEN' => $resultEN,
                'ErrorMessageTH' => $resultTH,
            ];
        else
            return array();
    }

    public function validateSaveProfile($request, bool $haveSavePassword = true)
    {
        $resultEN = array();
        $resultTH = array();
        $profileName = trim($request['ProfileName'], " ");
        $password = trim($request['Password'], " ");

        if (Common::IsNullOrEmptyString($profileName)) {
            array_push($resultEN, 'Profile Name is required.');
            array_push($resultTH, 'โปรดระบุชื่อโปรไฟล์');
        }

        if ($haveSavePassword) {
            if (Common::IsNullOrEmptyString($password)) {
                array_push($resultEN, 'Password is required.');
                array_push($resultTH, 'โปรดระบุรหัสผ่าน');
            } else if (strlen($password) < 8) {
                array_push($resultEN, 'Password less than 8 character.');
                array_push($resultTH, 'ระบุรหัสผ่านอย่างน้อย 8 ตัวอักษร');
            }
        }
        if (count($resultEN) > 0 || count($resultTH) > 0)
            return [
                'ErrorMessageEN' => $resultEN,
                'ErrorMessageTH' => $resultTH,
            ];
        else
            return array();
    }

    public function get(int $id)
    {
        $register = Register::find($id);
        $payments = Payment::where('ReferenceId', $id)->limit(1)->get();
        $payment = count($payments) > 0 ? $payments[0]  : null;
        $result =  array(
            'RegisterNo' => $register->RegisterNo,
            'ProfileName' => $register->ProfileName,
            'Login' => $register->Login,
            'Email' => $register->Email,
            'Peroid' => $register->PeroidSnapShot,
            'UnitPeroid' => UnitType::getKey($register->UnitPeroidSnapShot) . '(s)',
            "ActualPaid" => $payment != null ? $payment->ActualPaid  : '-',
            "Currency" =>  $payment != null ? $payment->Currency : '-',
        );
        return $result;
    }
}
