<?php

namespace App\Service;

use App\Models\PackagePrice;
use App\Models\CryptoCurrency;
use App\Constants\UnitType;

class PackagePriceService
{
    public function getall(BinanceProviderService $binanceService, int $pageIndex, int $pageSize, bool $notShowZeroPrice = false)
    {
        $packagePriceItems = PackagePrice::where('IsPublish', true)
            ->where(function ($query) use ($notShowZeroPrice) {
                if ($notShowZeroPrice === true)
                    $query->where('SetPrice', '!=', 0);
            })
            ->orderBy('SeqNo')
            ->offset($pageIndex * $pageSize)
            ->limit($pageSize)
            ->get();
        $results = array();
        $cryptoCurrencyItems = CryptoCurrency::all();
        foreach ($cryptoCurrencyItems as $cryptoCurrency) {
            $getCurrent = $binanceService->getCurrentAvgPrice($cryptoCurrency->SymbolUnitConvert);
            $coinMarketPrice = $getCurrent->ok() ? $getCurrent['price'] : 0;
            $data = array(
                "CoinNameId" => $cryptoCurrency->Id,
                "CoinSymbol" => $cryptoCurrency->Symbol,
                "CoinName" => $cryptoCurrency->Name,
                "CoinAvgPrice" => $coinMarketPrice,
                "CoinUnitCode" => UnitType::getKey(UnitType::USD),
            );
            $dataItems = array();
            foreach ($packagePriceItems as $packagePrice) {
                $dataItem = array(
                    'Id' => $packagePrice->Id,
                    'SeqNo' => $packagePrice->SeqNo,
                    'NameTH' => $packagePrice->NameTH,
                    'NameEN' => $packagePrice->NameEN,
                    'Peroid' => $packagePrice->Peroid,
                    'UnitPeroid' => UnitType::getKey($packagePrice->UnitPeroid),
                    'SetPrice' => $packagePrice->SetPrice,
                    'UnitCode' => UnitType::getKey($packagePrice->UnitCode),
                    'IsPublish' => $packagePrice->IsPublish,
                    "CoinSetPrice" => $packagePrice->SetPrice / $coinMarketPrice,
                    "CoinUnitCode" => $cryptoCurrency->Symbol,
                );
                array_push($dataItems, $dataItem);
            }
            $data["Pricing"] = $dataItems;
            array_push($results, $data);
        }
        return $results;
    }

    public function get(int $id)
    {
        return PackagePrice::find($id);
    }
}
