<?php

namespace App\Service;

use App\Models\CryptoForecast;
use App\Models\CryptoForecastItem;
use App\Constants\Common;
use App\Constants\UnitType;
use App\Http\Resources\CryptoForecastItemCollection;

class CryptoForecastingService
{
    public function findCurrent(string $symbol, int $pageIndex, int $pageSize, BinanceProviderService $binanceService)
    {
        $forecasts = CryptoForecast::where('IsPublish', true)
            ->where(function ($query) use ($symbol) {
                if (Common::IsNullOrEmptyString($symbol) == false)
                    $query->where('SymbolSnapShot', $symbol);
            })
            ->orderBy('SymbolSnapShot')
            ->offset($pageIndex * $pageSize)
            ->limit($pageSize)
            ->get();

        $forecastIds = $forecasts->pluck('Id');
        $currentDate = date("Y-m-d");
        $limitDate = date('Y-m-d', strtotime('+6 days'));
        $forecastItems = CryptoForecastItem::whereBetween('ForcastDate', [$currentDate, $limitDate])
            ->whereIn('CryptoForecastId', $forecastIds)
            ->get();
        $results = array();
        foreach ($forecasts as $forecast) {
            $getCurrent = $binanceService->getCurrentAvgPrice($forecast->SymbolUnitConvSnapShot);
            $coinMarketPrice = $getCurrent->ok() ? $getCurrent['price'] : 0;
            $forecastItemFilters = count($forecastItems) > 0
                ?  $forecastItems->toQuery()->where('CryptoForecastId', $forecast->Id)->get()
                : [];
            $data = array(
                "ForecastId" => $forecast->Id,
                "CoinSymbol" => $forecast->SymbolSnapShot,
                "CurrentDate" => $currentDate,     
                "CoinAvgPrice" => $coinMarketPrice,
                "CoinUnitCode" => UnitType::getKey(UnitType::USD),
                "ForecastItems" => CryptoForecastItemCollection::collection($forecastItemFilters)
            );
            array_push($results, $data);
        }
        return $results;
    }
}
