<?php

namespace App\Service;

use App\Models\CryptoCurrency;
use App\Http\Resources\CryptoCurrencyCollection;

class CryptoCurrencyService
{
    public function getall(int $pageIndex, int $pageSize)
    {
        $results = CryptoCurrency::orderBy('Symbol')
            ->offset($pageIndex * $pageSize)
            ->limit($pageSize)
            ->get();
        return CryptoCurrencyCollection::collection($results);
    }

    public function get(int $id)
    {
        return CryptoCurrency::find($id);
    }
}
