<?php

namespace App\Service;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Cache;
use App\Constants\Common;
use Exception;

class BinanceProviderService
{
    public $binanceBaseURL;
    public $binanceApiKey;
    public $binanceApiSecret;
    public function __construct()
    {
        $this->binanceBaseURL = env('BINANCE_BASE_URL');
        $this->binanceApiKey = env('BINANCE_API_KEY');
        $this->binanceApiSecret = env('BINANCE_API_SECRET');
    }

    function getBinanceAuthedFactory($pathUrl, $request)
    {
        $params = $request;
        $getServerTimeResult = $this->getServerTime();
        $serverTime = $getServerTimeResult->ok() ? $getServerTimeResult['serverTime'] : date_timestamp_get(now(7));
        $timestamp = 'timestamp=' . $serverTime;
        $params = Common::IsNullOrEmptyString($params) ? $timestamp : $params . '&' . $timestamp;
        $signature = hash_hmac('sha256', $params, $this->binanceApiSecret);
        $params =  $params . '&signature=' . $signature;
        $response = Http::withHeaders([
            'Content-Type' => 'application/json',
            'X-MBX-APIKEY' => $this->binanceApiKey,
        ])->get($this->binanceBaseURL . $pathUrl . '?' . $params);

        return $response;
    }

    public function getCurrentAvgPrice($symbol)
    {
        $response = Http::withHeaders([
            'Content-Type' => 'application/json',
        ])->get($this->binanceBaseURL . '/api/v3/avgPrice?symbol=' . $symbol);
        return $response;
    }

    public function getServerTime()
    {
        $response = Http::withHeaders([
            'Content-Type' => 'application/json',
        ])->get($this->binanceBaseURL . '/api/v3/time');
        return $response;
    }

    public function getPayTransactions($startTime, $endTime)
    {
        $params = "";
        if ($startTime != null) {
            $param = 'startTimestamp=' . date_timestamp_get($startTime) . '000';
            $params = Common::IsNullOrEmptyString($params) ? $param : $params . '&' . $param;
        }
        if ($endTime != null) {
            $param = 'endTimestamp=' . date_timestamp_get($endTime) . '000';
            $params = Common::IsNullOrEmptyString($params) ? $param : $params . '&' . $param;
        }
        $limit = 'limit=100';
        $params = Common::IsNullOrEmptyString($params) ? $limit : $params . '&' . $limit;
        return $this->getBinanceAuthedFactory('/sapi/v1/pay/transactions', $params);
    }

    public function getAllConfig($coinSymbol)
    {
        $cacheName = 'bConfig';
        $configItems = [];
        if (Cache::store('file')->has($cacheName)) {
            $configItems = Cache::store('file')->get($cacheName);
        } else {
            $response =  $this->getBinanceAuthedFactory('/sapi/v1/capital/config/getall', null);
            if ($response->ok()) {
                $configItems = json_decode($response, true);
                $expiredTime = now(7)->addHours(12);
                Cache::store('file')->add($cacheName, $configItems, $expiredTime);
            } else
                throw new Exception('Binance API Error.||การเชื่อมต่อ Binance ไม่สมบูรณ์');
        }

        $result = collect($configItems)->where('coin', '=', $coinSymbol)->first();
        if ($result == null)
            throw new Exception('Coin config not found.||ไม่พบการตั้งค่าของเหรียญ');

        $results = array();
        foreach ($result['networkList'] as $networkData) {
            $dataItem = array(
                'Coin' => $networkData['coin'],
                'Network' => $networkData['network'],
                'Name' => $networkData['name'],
                'MinConfirm' => $networkData['minConfirm'],
                'DepositEnable' => $networkData['depositEnable'],
            );
            array_push($results, $dataItem);
        }
        return $results;
    }

    public function getDepositAddress($coinSymbol, $network)
    {
        $params = "coin=" . $coinSymbol;
        if ($coinSymbol != null) {
            $param = 'network=' . $network;
            $params = Common::IsNullOrEmptyString($params) ? $param : $params . '&' . $param;
        }
        return $this->getBinanceAuthedFactory('/sapi/v1/capital/deposit/address', $params);
    }

    public function getDepositHistory($coinSymbol, $startTime, $endTime, $status)
    {
        $params = "coin=" . $coinSymbol;
        if ($startTime != null) {
            //  Default: 90 days from current timestamp
            $param = 'startTime=' . date_timestamp_get($startTime) . '000';
            $params = Common::IsNullOrEmptyString($params) ? $param : $params . '&' . $param;
        }
        if ($endTime != null) {
            //  Default: present timestamp
            $param = 'endTime=' . date_timestamp_get($endTime) . '000';
            $params = Common::IsNullOrEmptyString($params) ? $param : $params . '&' . $param;
        }
        if ($status != null) {
            // 	0(0:pending,6: credited but cannot withdraw, 1:success)
            $param = 'status=' . $status;
            $params = Common::IsNullOrEmptyString($params) ? $param : $params . '&' . $param;
        }

        return $this->getBinanceAuthedFactory('/sapi/v1/capital/deposit/hisrec', $params);
    }
}
