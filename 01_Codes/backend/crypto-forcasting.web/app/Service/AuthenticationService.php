<?php

namespace App\Service;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Mail;
use App\Constants\Common;
use App\Constants\AuthResult;
use App\Models\User;
use App\Mail\IdentityVerifier;
use Exception;

class AuthenticationService
{
    function login($userLogin,  $userPassword)
    {
        $result = AuthResult::NotActivated;
        if (Common::IsNullOrEmptyString($userLogin)) {
            throw new Exception('Login is required.||โปรดระบุล็อกอิน');
        }
        if (Common::IsNullOrEmptyString($userPassword)) {
            throw new Exception('Password is required.||โปรดระบุรหัสผ่าน');
        }

        $users = User::where('IsActive', true)
            ->where('Login', $userLogin)
            ->limit(1)->get();
        $existUser = count($users) > 0 ? $users[0] : null;

        if ($existUser !== null) {
            if (Hash::check($userPassword, $existUser->Password)) {
                if ($existUser->ExpiredDate != null && $existUser->ExpiredDate <= now(7))
                    $result = AuthResult::UserExpired;
                else
                    $result = AuthResult::Success;
            } else {
                $result = AuthResult::Invalid;
            }
        }

        return  [
            'ProfileName' => ($existUser !== null ? $existUser->ProfileName : ''),
            'PrivateKey' => ($existUser !== null ? $existUser->PrivateKey : ''),
            'Email' => ($existUser !== null ? $existUser->Email : ''),
            'LoginResult' =>  $result
        ];
    }

    function forgotPassword($email)
    {
        $users = User::where('Email', '=', $email)
            ->limit(1)->get();
        if (count($users) > 0) {
            $user = $users[0];
            $refKey = uniqid();
            $verifyKey = $user->PrivateKey . $refKey;
            $sessionLinkTime = env('SESSION_LINK_MINUTE_TIME') != null ? env('SESSION_LINK_MINUTE_TIME') : 5;
            $expiredTime =  now(7)->addMinute($sessionLinkTime);
            Cache::store('linkEmail')->add($verifyKey, $user->Id, $expiredTime);
            Cache::store('dupRequest')->add($user->Email, $verifyKey, $expiredTime);
            Mail::to($user->Email)->send(new IdentityVerifier($user, $sessionLinkTime, $verifyKey, $refKey));
            return $refKey;
        } else {
            throw new Exception('User not found !||ไม่พบผู้ใช้งานในระบบ');
        }
    }

    function resetPassword($verifyKey, $userId, $newPassword)
    {
        $user = User::find($userId);
        if ($user !== null) {
            $user->Password = Hash::make($newPassword);
            $user->save();
            Cache::store('linkEmail')->forget($verifyKey);
            Cache::store('dupRequest')->forget($user->Email);
        } else {
            throw new Exception('User not found !||ไม่พบผู้ใช้งานในระบบ');
        }
    }
}
