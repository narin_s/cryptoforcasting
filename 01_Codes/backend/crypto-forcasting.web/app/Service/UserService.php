<?php

namespace App\Service;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Constants\PaymentType;
use App\Models\User;
use App\Mail\otpVerifier;
use Exception;

class UserService
{
    function getUserProfile($privateKey)
    {
        $users = User::where('PrivateKey', $privateKey)->limit(1)->get();
        return count($users) > 0 ? $users[0] : null;
    }

    function requestOTP($privateKey, $sessionLinkTime)
    {
        $user = $this->getUserProfile($privateKey);
        if ($user != null) {
            $refKey = uniqid();
            $expiredTime =  now(7)->addMinute($sessionLinkTime);
            $otp = mt_rand(1000, 9999);
            Cache::store('linkEmail')->add($user->PrivateKey, $otp, $expiredTime);
            Mail::to($user->Email)->send(new otpVerifier($user, $sessionLinkTime, $refKey, $otp));
            return $refKey;
        } else {
            throw new Exception('User not found !||ไม่พบผู้ใช้งานในระบบ');
        }
    }

    function saveUserProfile($request, $saveNewPassword, RegisterService $registerService)
    {
        $validateResults =  $registerService->validateSaveProfile($request, $saveNewPassword);
        if (count($validateResults) > 0) {
            return response()->json([
                'ErrorMessage' => $validateResults
            ], 400);
        }
        $userId = $request['UserId'];
        $user = User::find($userId);
        $password = trim($request['Password'], " ");
        $newPassword = trim($request['NewPassword'], " ");
        if ($user !== null) {
            $user->ProfileName =  $request['ProfileName'];
            if ($saveNewPassword === true) {
                if (Hash::check($password, $user->Password))
                    $user->Password = Hash::make($newPassword);
                else
                    throw new Exception('Old Password not match !||รหัสผ่านเดิมไม่ถูกต้อง');
            }
            $user->save();
            Cache::store('linkEmail')->forget($user->PrivateKey);
            return $user->Id;
        } else {
            throw new Exception('User not found !||ไม่พบผู้ใช้งานในระบบ');
        }
    }

    function renewAccount($request, $paymentData, $packagePrice, $cryptoCurrency, RegisterService $registerService)
    {
        $loginProfile = cache($request['AccessToken']);
        $user = $this->getUserProfile($loginProfile['PrivateKey']);
        if ($user == null) throw new Exception('User not found !||ไม่พบผู้ใช้งานในระบบ');
        $exchangeRate =  $packagePrice->SetPrice / $request['CryptoSetPrice'];

        try {
            DB::beginTransaction();
            $currentDate = now(7);
            $newExpiredDate = $currentDate > $user->ExpiredDate ? $currentDate : $user->ExpiredDate;
            $user->ExpiredDate = $registerService->getExpiredDate($newExpiredDate, $packagePrice);
            $user->save();
            $registerService->savePayment(
                array(
                    'PaymentType' => PaymentType::Renew,
                    'RefId'  => $user->Id,
                    'SetPrice' => $packagePrice->SetPrice,
                    'UnitCode' => $packagePrice->UnitCode,
                    'Symbol' => $cryptoCurrency->Symbol,
                    'ExchangeRate' => $exchangeRate,
                    'CryptoSetPrice' => $request['CryptoSetPrice'],
                    'OrderType' => $paymentData['orderType'],
                    'TransactionId' => $paymentData['transactionId'],
                    'TransactionTime' => $paymentData['transactionTime'],
                    'Amount' => $paymentData['amount'],
                    'Currency' => $paymentData['currency'],
                )
            );

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }
}
