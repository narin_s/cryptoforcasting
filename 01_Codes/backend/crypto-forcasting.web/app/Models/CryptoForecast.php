<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CryptoForecast extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'CryptoId',
        'SymbolSnapShot',
        'SymbolUnitConvSnapShot',
        'IsPublish',
    ];

    protected $casts = [
        'CreatedDate' => 'datetime',
        'UpdatedDate' => 'datetime',
    ];

    protected $primaryKey = 'Id';
    protected $table = 'CryptoForecast';

    public function forecastItems()
    {
        return CryptoForecastItem::where('CryptoForecastId', $this->Id)->get();
        // return $this->hasMany(CryptoForecastItem::class, 'CryptoForecastId', 'Id');
    }

    // public function latestForecastItem()
    // {
    //     return $this->hasOne(CryptoForecastItem::class)->latestOfMany();
    // }
}
