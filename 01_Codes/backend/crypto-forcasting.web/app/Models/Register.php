<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Register extends BaseModel
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'RegisterNo',
        'ProfileName',
        'UserId',
        'Login',
        'SecretKey',
        'Email',
        'Status',
        'PeroidSnapShot',
        'UnitPeroidSnapShot',
    ];

    protected $hidden = [
        'SecretKey'
    ];

    protected $table = 'Register';
}
