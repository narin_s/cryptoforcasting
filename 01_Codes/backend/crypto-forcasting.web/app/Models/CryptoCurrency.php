<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CryptoCurrency extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'Symbol',
        'Name',
        'SymbolUnitConvert',
    ];
    
    protected $primaryKey = 'Id';
    protected $table = 'CryptoCurrency';
}
