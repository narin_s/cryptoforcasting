<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Constants\PaymentType;

class Payment extends BaseModel
{
    use HasFactory;
    use SoftDeletes;

    // public function __construct(PaymentType $type)
    // {
    //     $this->attributes['Type'] = $type;
    // }

    protected $fillable = [
        'Type',
        'ReferenceId',
        'SetPriceSnapShot',
        'UnitCodeSnapShot',
        'SymbolSnapShot',
        'ExchangeRate',
        'TotalPaid',
        'OrderType',
        'TransactionId',
        'TransactionDate',
        'ActualPaid',
        'Currency',
    ];

    protected $hidden = [
        'OrderType',
        'TransactionId',
        'TransactionDate',
        'ActualPaid',
        'Currency'
    ];

    protected $table = 'Payment';
}
