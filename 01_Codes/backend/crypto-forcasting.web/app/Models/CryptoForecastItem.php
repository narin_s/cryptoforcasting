<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Constants\SuggestStatus;

class CryptoForecastItem extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'CryptoForecastId',
        'Type',
        'SuggestStatus',
        'ForcastDate',
    ];

    protected $casts = [
        'CreatedDate' => 'datetime',
        'UpdatedDate' => 'datetime',
    ];

    protected $primaryKey = 'Id';
    protected $table = 'CryptoForecastItem';

    public function CryptoForecast()
    {
        return CryptoForecast::find($this->CryptoForecastId);
        // return $this->belongsTo(CryptoForecast::class);
    } 
    
    public function ForecastSymbol()
    {
        return CryptoForecast::find($this->CryptoForecastId)->SymbolSnapShot;
        // return $this->belongsTo(CryptoForecast::class);
    }
}
