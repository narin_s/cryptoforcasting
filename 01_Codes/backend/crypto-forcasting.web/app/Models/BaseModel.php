<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
    const CREATED_AT = 'CreatedDate';
    const UPDATED_AT = 'UpdatedDate';
    const DELETED_AT = 'DeletedDate';

    protected $casts = [
        'CreatedDate' => 'datetime',
        'UpdatedDate' => 'datetime',
        'DeletedDate' => 'datetime',
    ];

    protected $primaryKey = 'Id';
}