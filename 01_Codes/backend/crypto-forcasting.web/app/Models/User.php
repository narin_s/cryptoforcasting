<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends BaseModel
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'ProfileName',
        'Email',
        'Login',
        'Password',
        'PrivateKey',
        'IsActive',
        'ExpiredDate',
    ];

    protected $hidden = [
        'Password',
        'IsActive',
        'CreatedDate',
        'UpdatedDate',
        'DeletedDate',
    ];

    protected $casts = [
        'ExpiredDate' => 'datetime',
    ];

    protected $table = 'User';
}
