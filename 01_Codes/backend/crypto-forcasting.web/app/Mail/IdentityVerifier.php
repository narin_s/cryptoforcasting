<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\User;

class IdentityVerifier extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $lifetime;
    public $url;
    public $refKey;
    public function __construct(User $iUser, $lifetime, $verifyKey, $refKey)
    {
        $this->user = $iUser;
        $this->lifetime = $lifetime;
        $this->url = env('REGT_CONFIRM_URL') . '?verifyKey=' . $verifyKey;
        $this->refKey = $refKey;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.identityVerifier');
    }
}
