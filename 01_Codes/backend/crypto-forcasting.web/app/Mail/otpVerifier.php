<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\User;

class otpVerifier extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $lifetime;
    public $refKey;
    public $otp;
    public function __construct(User $iUser, $lifetime, $refKey, $otp)
    {
        $this->user = $iUser;
        $this->lifetime = $lifetime;
        $this->refKey = $refKey;
        $this->otp = $otp;
    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.otpVerifier');
    }
}
