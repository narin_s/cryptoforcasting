<?php

namespace App\Constants;

use BenSampo\Enum\Enum;

final class SuggestStatus extends Enum
{
    const StrongDecending = 88;
    const Decending = 89;
    const Neutral = 90;
    const Ascending = 91;
    const StrongAscending = 92;


    // public function getText($locate): string
    // {
    //     if ($locate === 'th' || $locate === 'TH')
    //         return match ($this) {
    //             SuggestStatus::StrongBuy => 'Strong Buy',
    //             SuggestStatus::Buy => 'Buy',
    //             SuggestStatus::Neutral => 'Neutral',
    //             SuggestStatus::Sell => 'Sell',
    //             SuggestStatus::StrongSell => 'Strong Sell',
    //         };
    //     else
    //         return match ($this) {
    //             SuggestStatus::StrongBuy => 'Strong Buy',
    //             SuggestStatus::Buy => 'Buy',
    //             SuggestStatus::Neutral => 'Neutral',
    //             SuggestStatus::Sell => 'Sell',
    //             SuggestStatus::StrongSell => 'Strong Sell',
    //         };
    // }
}
