<?php

namespace App\Constants;

class Common
{
    public static function IsNullOrEmptyString($str)
    {
        return (!isset($str) || trim($str) === '');
    }
}