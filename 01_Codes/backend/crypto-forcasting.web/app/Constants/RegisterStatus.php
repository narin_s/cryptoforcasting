<?php

namespace App\Constants;

use BenSampo\Enum\Enum;

final class RegisterStatus extends Enum
{
    const Draft = 60;
    const Commit = 61;
    const Cancel = 62;
}
