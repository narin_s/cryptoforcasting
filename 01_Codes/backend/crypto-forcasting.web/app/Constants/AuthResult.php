<?php

namespace App\Constants;

use BenSampo\Enum\Enum;

final class AuthResult extends Enum
{
    const Success = 101;
    const Invalid = 102;
    const UserExpired = 103;
    const NotActivated = 104;
}
