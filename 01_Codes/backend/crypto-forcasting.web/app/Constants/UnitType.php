<?php

namespace App\Constants;

use BenSampo\Enum\Enum;

final class UnitType extends Enum
{
    const Day = 11;
    const Week = 12;
    const Month = 13;
    const Year = 14;

    const THB = 50;
    const USD = 51;
}
