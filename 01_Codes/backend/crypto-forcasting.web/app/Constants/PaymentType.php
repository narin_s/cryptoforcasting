<?php

namespace App\Constants;

use BenSampo\Enum\Enum;

final class PaymentType extends Enum
{
    const Register = 21;
    const Renew = 22;
}
