<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Constants\UnitType;

class PackagePriceCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'Id' => $this->Id,
            'SeqNo' => $this->SeqNo,
            'NameTH' => $this->NameTH,
            'NameEN' => $this->NameEN,
            'Peroid' => $this->Peroid,
            'UnitPeroid' => UnitType::getKey($this->UnitPeroid),
            'SetPrice' => $this->SetPrice,
            'UnitCode' => UnitType::getKey($this->UnitCode),
            'IsPublish' => $this->IsPublish,
        ];
    }
}
