<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Constants\UnitType;

class RegisterCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'RegisterNo' => $this->RegisterNo,
            'ProfileName' => $this->ProfileName,
            'Login' => $this->Login,
            'Email' => $this->Email,
            'Peroid' => $this->PeroidSnapShot,
            'UnitPeroid' => UnitType::getKey($this->UnitPeroidSnapShot),
        ];
    }
}
