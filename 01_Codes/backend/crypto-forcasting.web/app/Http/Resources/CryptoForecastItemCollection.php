<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Constants\SuggestStatus;

class CryptoForecastItemCollection extends JsonResource
{
    /**
 * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'Id' => $this->Id,
            'Type' => $this->Type,
            'SuggestStatus' => $this->SuggestStatus,
            'SuggestText' => SuggestStatus::getKey($this->SuggestStatus),
            'ForcastDate' => $this->ForcastDate,
        ];
    }
}
