<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use App\Service\PackagePriceService;
use App\Service\BinanceProviderService;

class PackagePriceController extends Controller
{
    function getall(Request $request, PackagePriceService $service, BinanceProviderService $binanceService)
    {
        $criteria = $request->json('criteria');
        $pageIndex = isset($criteria['PageIndex']) ? $criteria['PageIndex'] : 0;
        $pageSize = isset($criteria['PageSize']) ? $criteria['PageSize'] : 20;
        $results = $service->getall($binanceService, $pageIndex, $pageSize);
        return response()
            ->json($results, 200)
            ->header('Content-Type', 'application/json');
    }

    function getForRenew(Request $request, PackagePriceService $service, BinanceProviderService $binanceService)
    {
        $token = $this->GetAuthToken($request);
        if (Cache::missing($token)) {
            return response(null, 401);
        }
        $criteria = $request->json('criteria');
        $pageIndex = isset($criteria['PageIndex']) ? $criteria['PageIndex'] : 0;
        $pageSize = isset($criteria['PageSize']) ? $criteria['PageSize'] : 20;
        $results = $service->getall($binanceService, $pageIndex, $pageSize, true);
        return response()
            ->json($results, 200)
            ->header('Content-Type', 'application/json');
    }
}
