<?php

namespace App\Http\Controllers;
use App\Service\CryptoCurrencyService;

use Illuminate\Http\Request;

class CryptoCurrencyController extends Controller
{
    function getall(Request $request, CryptoCurrencyService $service)
    {
        $criteria = $request->json('criteria');
        $pageIndex = isset($criteria['PageIndex']) ? $criteria['PageIndex'] : 0;
        $pageSize = isset($criteria['PageSize']) ? $criteria['PageSize'] : 20;
        $results = $service->getall($pageIndex, $pageSize);
        return response()
            ->json($results, 200)
            ->header('Content-Type', 'application/json');
    }
}
