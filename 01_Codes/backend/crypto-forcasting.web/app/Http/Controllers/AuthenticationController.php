<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use App\Constants\AuthResult;
use App\Constants\Common;
use App\Service\AuthenticationService;
use Exception;

class AuthenticationController extends Controller
{
    function login(Request $request, AuthenticationService $service)
    {
        $registerData = $request->json('data');
        #region Validate Request
        if (isset($registerData['Login']) === false) {
            data_fill($registerData, 'Login', '');
        }

        if (isset($registerData['Password']) === false) {
            data_fill($registerData, 'Password', '');
        }
        #endregion
        try {
            $result = $service->login($registerData['Login'], $registerData['Password']);
            $loginResultCode = $result['LoginResult'];
            $authenToken = '';
            if ($loginResultCode === AuthResult::Success) {
                $authenToken = $result['PrivateKey'] . uniqid();
                cache([$authenToken => $result], now(7)->addHours(8));
                return response()
                    ->json([
                        'LoginResult' => $loginResultCode,
                        'ProfileName' => $result['ProfileName'],
                        'Token' => $authenToken,
                    ], 200)
                    ->header('Content-Type', 'application/json');
            } else {
                return response()
                    ->json([
                        'LoginResult' => $loginResultCode,
                        'ProfileName' => $result['ProfileName'],
                        'Token' => $authenToken,
                    ], 400)
                    ->header('Content-Type', 'application/json');
            }
        } catch (Exception $ex) {
            return $this->InjectionErrorMessage($ex);
        }
    }

    function logout(Request $request)
    {
        $token = $this->GetAuthToken($request);
        if (Cache::missing($token)) {
            return response(null, 401);
        } else {
            Cache::forget($token);
            return response(null, 200);
        }
    }

    function getLoginProfile(Request $request)
    {
        $token = $this->GetAuthToken($request);
        $loginProfile = cache($token);
        return response()
            ->json(['ProfileData' => $loginProfile], 200)
            ->header('Content-Type', 'application/json');
    }

    function forgotPassword(Request $request, AuthenticationService $service)
    {
        $email = $request->json('email');
        #region Validate Request
        if (Common::IsNullOrEmptyString($email)) {
            return response()->json([
                'ErrorMessageEN' => ['Email is required !'],
                'ErrorMessageTH' => ['จำเป็นต้องระบุอีเมล !'],
            ], 400);
        }
        if (Cache::store('dupRequest')->has($email)) {
            return response()->json([
                'ErrorMessageEN' => ['Duplicate request, Wait for a few minutes'],
                'ErrorMessageTH' => ['ไม่สามารถร้องขอต่อเนื่องได้'],
            ], 400);
        }
        #endregion
        try {
            $refKey =  $service->forgotPassword($email);
            return response()
                ->json(['RefKey' => $refKey], 200)
                ->header('Content-Type', 'application/json');
        } catch (Exception $ex) {
            return $this->InjectionErrorMessage($ex);
        }
    }

    function resetPassword(Request $request, AuthenticationService $service)
    {
        $requestData = $request['data'];
        $verifyKey = $requestData['VerifyKey'];
        $newPassword = $requestData['NewPassword'];

        if (Cache::store('linkEmail')->has($verifyKey) === false) {
            return response()->json([
                'ErrorMessageEN' => ['Session of password reset has expired !'],
                'ErrorMessageTH' => ['เซสชั่นการรีเซ็ตรหัสผ่านหมดอายุ !'],
            ], 403);
        } else {
            if (Common::IsNullOrEmptyString($newPassword)) {
                return response()->json([
                    'ErrorMessageEN' => ['Password is required'],
                    'ErrorMessageTH' => ['จำเป็นต้องระบุรหัสผ่านใหม่'],
                ], 400);
            } else if (strlen($newPassword) < 8) {
                return response()->json([
                    'ErrorMessageEN' => ['Password less than 8 character'],
                    'ErrorMessageTH' => ['รหัสผ่านต้องมีอย่างน้อย 8 อักษร'],
                ], 400);
            }
            try {
                $userId  = Cache::store('linkEmail')->get($verifyKey);
                $service->resetPassword($verifyKey, $userId, $newPassword);
                return response()->json([
                    'MessageEN' => ['Reset Your Password Successfully'],
                    'MessageTH' => ['รีเซ็ตรหัสผ่านสำเร็จ'],
                ], 200)->header('Content-Type', 'application/json');
            } catch (Exception $ex) {
                return $this->InjectionErrorMessage($ex);
            }
        }
    }
}
