<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;
use App\Constants\Common;
use App\Service\UserService;
use App\Service\RegisterService;
use App\Service\PackagePriceService;
use App\Service\CryptoCurrencyService;
use App\Service\BinanceProviderService;
// use App\Mail\IdentityVerifier;
use App\Models\Payment;
use Exception;

class UserProfileController extends Controller
{
    #region SaveProfile
    function getUserProfile(Request $request, UserService $service)
    {
        $token = $this->GetAuthToken($request);
        if (Cache::missing($token)) {
            return response(null, 401);
        }

        $loginProfile = cache($token);
        $user = $service->getUserProfile($loginProfile['PrivateKey']);
        if ($user !== null) {
            return response()
                ->json($user, 200)
                ->header('Content-Type', 'application/json');
        } else {
            return response()->json([
                'ErrorMessageEN' => ['User profile not found.'],
                'ErrorMessageTH' => ['ไม่พบโปรไฟล์ผู้ใช้งาน'],
            ], 400);
        }
    }

    function requestOTP(Request $request, UserService $service)
    {
        $token = $this->GetAuthToken($request);
        if (Cache::missing($token)) {
            return response(null, 401);
        } else {
            $loginProfile = cache($token);
            $sessionLinkTime = env('SESSION_LINK_MINUTE_TIME') != null ? env('SESSION_LINK_MINUTE_TIME') : 5;
            #region Validate Request
            if (Cache::store('linkEmail')->has($loginProfile['PrivateKey'])) {
                return response()->json([
                    'ErrorMessageEN' => ['Duplicate request, Wait for a ' . $sessionLinkTime . ' minutes'],
                    'ErrorMessageTH' => ['ไม่สามารถร้องขอต่อเนื่องได้ ภายใน' . $sessionLinkTime . 'นาที'],
                ], 400);
            }
            #endregion
            try {
                $refKey = $service->requestOTP($loginProfile['PrivateKey'], $sessionLinkTime);
                return response()
                    ->json(['RefKey' => $refKey], 200)
                    ->header('Content-Type', 'application/json');
            } catch (Exception $ex) {
                return $this->InjectionErrorMessage($ex);
            }
        }
    }

    function saveUserProfile(Request $request, UserService $service, RegisterService $registerService)
    {
        $requestData = $request->json('data');
        if ($requestData !== null) {
            #region Validate Request
            if (isset($requestData['ProfileName']) === false) {
                data_fill($requestData, 'ProfileName', '');
            }

            if (isset($requestData['PrivateKey']) === false) {
                data_fill($requestData, 'PrivateKey', '');
            }

            $token = $this->GetAuthToken($request);
            if (Cache::missing($token)) {
                return response(null, 401);
            }

            $loginProfile = cache($token);
            if ($loginProfile['PrivateKey'] !== $requestData['PrivateKey']) {
                return response()->json([
                    'ErrorMessageEN' => ['Access denied.'],
                    'ErrorMessageTH' => ['ปฏิเสธการเข้าใช้'],
                ], 403);
            }
            $saveNewPassword = Common::IsNullOrEmptyString($requestData['SaveNewPassword']) ? false : $requestData['SaveNewPassword'];
            if ($saveNewPassword) {
                $otp = isset($requestData['OTP']) ? $requestData['OTP'] : '';
                if (Common::IsNullOrEmptyString($otp)) {
                    return response()->json([
                        'ErrorMessageEN' => ['OTP is required.'],
                        'ErrorMessageTH' => ['จำเป็นต้องระบุรหัส OTP'],
                    ], 400);
                }

                $otpInCache = Cache::store('linkEmail')->get($loginProfile['PrivateKey']);
                if ($otpInCache == null) {
                    return response()->json([
                        'ErrorMessageEN' => ['OTP is require'],
                        'ErrorMessageTH' => ['จำเป็นต้องร้องขอ OTP'],
                    ], 400);
                }
                if ($otpInCache !== $otp) {
                    return response()->json([
                        'ErrorMessageEN' => ['OTP not match'],
                        'ErrorMessageTH' => ['รหัส OTP ไม่ตรงกัน'],
                    ], 400);
                }
            }
            #endregion
            try {
                $userId = $service->saveUserProfile($requestData,  $saveNewPassword, $registerService);
                return response()
                    ->json(['UserId' => $userId], 200)
                    ->header('Content-Type', 'application/json');
            } catch (Exception $ex) {
                return $this->InjectionErrorMessage($ex);
            }
        } else {
            return response()->json([
                'ErrorMessageEN' => ['Request not found.'],
                'ErrorMessageTH' => ['รูปแบบคำร้องไม่ถูกต้อง'],
            ], 400);
        }
    }
    #endregion

    #region Internal Chain Payment
    function getpaymentMethod(Request $request, RegisterService $service)
    {
        $token = $this->GetAuthToken($request);
        if (Cache::missing($token)) {
            return response(null, 401);
        }

        $result = $service->getpaymentMethod($request);
        return response()
            ->json($result, 200)
            ->header('Content-Type', 'application/json');
    }

    function renewAccountInternalChain(
        Request $request,
        UserService $service,
        RegisterService $registerService,
        PackagePriceService $packagePriceService,
        CryptoCurrencyService $cryptoCurrencyService,
        BinanceProviderService $binanceService
    ) {
        $token = $this->GetAuthToken($request);
        if (Cache::missing($token)) {
            return response(null, 401);
        }

        #region PackagePrice&CryptoCurrency Verify
        $packagePrice = $packagePriceService->get($request['PackagePriceId']);
        $cryptoCurrency = $cryptoCurrencyService->get($request['CryptoCurrencyId']);
        if ($packagePrice == null  || $cryptoCurrency == null) {
            return response()->json([
                'ErrorMessageEN' => ['PackagePrice or CryptoCurrency not found.'],
                'ErrorMessageTH' => ['ไม่พบตารางราคาหรือข้อมูลเหรียญคริปโต'],
            ], 400);
        }

        if (
            $request['CryptoSetPrice'] <= 0 || $packagePrice->SetPrice <= 0
            || Common::IsNullOrEmptyString($request['CryptoUnitCode'])
        ) {
            return response()->json([
                'ErrorMessageEN' => ['[CryptoSetPrice] Cannot Equal 0 and [CryptoUnitCode] Cannot Be NULL'],
                'ErrorMessageTH' => ['[ราคาของเหรียญคริปโต]ต้องไม่เท่ากับ 0 และ [หน่วยของเหรียญคริปโต]ต้องไม่เป็นค่าว่าง'],
            ], 400);
        }
        #endregion

        #region Binance Payment Transaction Verify
        $prefixTranId = $request['PrefixTransactionId'];
        $suffixTranId = $request['SuffixTransactionId'];

        $sessionRegTime = env('SESSION_REGISTER_MINUTE_TIME');
        $current = now(7);
        $startTimestamp = $current->addMinute($sessionRegTime * -1);
        $endTimestamp = $current;
        $getPayTranResponse =  $binanceService->getPayTransactions($startTimestamp, $endTimestamp);
        if ($getPayTranResponse->ok() == false) {
            return response()->json([
                'ErrorMessageEN' => ['Binance Connection Error !'],
                'ErrorMessageTH' => ['เกิดข้อผิดพลาดในการเชื่อมต่อ Binance'],
            ], 400);
        }
        $transactions = collect($getPayTranResponse["data"]);
        $verifyResult = $transactions->where('amount', '>', 0)
            ->where('currency', $request["CryptoUnitCode"])
            ->filter(function ($q) use ($prefixTranId, $suffixTranId) {
                return Str::startsWith($q['transactionId'], 'C_R_' . $prefixTranId)
                    && Str::endsWith($q['transactionId'],  $suffixTranId);
            })->first();

        if ($verifyResult == null) {
            return response()->json([
                'ErrorMessageEN' => ['Verify Result : Payment Transaction Not Found'],
                'ErrorMessageTH' => ['ผลการตรวจสอบ : ไม่พบการชำระเงิน'],
            ], 403);
        }

        $diffAcceptPercent = env('DIFF_ACCEPT_PERCENT');
        $verifyAmount = $verifyResult['amount'];
        $cryptoSetPric = $request['CryptoSetPrice'];
        $minLimit = $cryptoSetPric * (1 - ($diffAcceptPercent / 100));
        if ($verifyAmount < $minLimit) {
            return response()->json([
                'ErrorMessageEN' => ['Verify Result : Total Payment is unsuccess'],
                'ErrorMessageTH' => ['ผลการตรวจสอบ : ยอดชำระไม่ครบถ้วน'],
            ], 400);
        }
        #endregion

        #region Internal Payment Used Verify
        $existPayments = Payment::where('TransactionId', $verifyResult['transactionId'])->limit(1)->get();
        if (count($existPayments) > 0) {
            return response()->json([
                'ErrorMessageEN' => ['Verify Result : Payment Transaction is used'],
                'ErrorMessageTH' => ['ผลการตรวจสอบ : การชำระเงินได้นำไปใช้งานแล้ว'],
            ], 403);
        }
        #endregion

        try {
            data_fill($request, 'AccessToken',  $token);
            $service->renewAccount($request, $verifyResult, $packagePrice, $cryptoCurrency, $registerService);
            return response()->json([
                'MessageEN' => ['Account renew is successfully'],
                'MessageTH' => ['ต่ออายุบัญชีเสร็จสิ้น'],
            ], 200);
        } catch (Exception $ex) {
            return $this->InjectionErrorMessage($ex);
        }
    }
    #endregion

    #region External Chain Payment
    function getAllConfig(Request $request, BinanceProviderService $service)
    {
        $token = $this->GetAuthToken($request);
        if (Cache::missing($token)) {
            return response(null, 401);
        }

        $coinSymbol = $request->json('CoinSymbol');
        #region Validate Request
        if (isset($coinSymbol) === false || Common::IsNullOrEmptyString($coinSymbol)) {
            return response()->json([
                'ErrorMessageEN' => ['[CoinSymbol] : Coin is required for deposit address.'],
                'ErrorMessageTH' => ['[CoinSymbol] : ต้องระบุเหรียญในการร้องขอข้อมูลการตั้งค่า'],
            ], 400);
        }
        #endregion

        try {
            $resData = $service->getAllConfig($coinSymbol);
            return $resData;
        } catch (Exception $ex) {
            return $this->InjectionErrorMessage($ex);
        }
    }

    function getDepositAddress(Request $request, BinanceProviderService $service)
    {
        $token = $this->GetAuthToken($request);
        if (Cache::missing($token)) {
            return response(null, 401);
        }

        $requestData = $request->json('data');
        #region Validate Request
        if (isset($requestData['CoinSymbol']) === false) {
            return response()->json([
                'ErrorMessageEN' => ['[CoinSymbol] : Coin is required for deposit address.'],
                'ErrorMessageTH' => ['[CoinSymbol] : ต้องระบุเหรียญในการร้องขอที่อยู่การรับโอน'],
            ], 400);
        }
        if (isset($requestData['Network']) === false) {
            data_fill($requestData, 'Network', null);
        }
        #endregion
        $response = $service->getDepositAddress($requestData['CoinSymbol'], $requestData['Network']);
        if ($response->ok()) {
            $resData = $response->json();
            return response()
                ->json([
                    "CoinSymbol" => $resData["coin"],
                    "Address" => $resData["address"],
                    "Tag" => $resData["tag"],
                    "Url" => $resData["url"],
                ], 200)
                ->header('Content-Type', 'application/json');
        } else {
            return response()->json([
                'ErrorMessageEN' => ['Binance API Error.'],
                'ErrorMessageTH' => ['การเชื่อมต่อ Binance ไม่สมบูรณ์'],
            ], 400);
        }
    }

    function renewAccountExternalChain(
        Request $request,
        UserService $service,
        RegisterService $registerService,
        PackagePriceService $packagePriceService,
        CryptoCurrencyService $cryptoCurrencyService,
        BinanceProviderService $binanceService
    ) {
        $token = $this->GetAuthToken($request);
        if (Cache::missing($token)) {
            return response(null, 401);
        }

        #region PackagePrice&CryptoCurrency Verify
        $packagePrice = $packagePriceService->get($request['PackagePriceId']);
        $cryptoCurrency = $cryptoCurrencyService->get($request['CryptoCurrencyId']);
        if ($packagePrice == null || $cryptoCurrency == null) {
            return response()->json([
                'ErrorMessageEN' => ['PackagePrice or CryptoCurrency not found.'],
                'ErrorMessageTH' => ['ไม่พบตารางราคาหรือข้อมูลเหรียญคริปโต'],
            ], 400);
        }

        if (
            ($request['CryptoSetPrice'] <= 0 || $packagePrice->SetPrice <= 0)
            || Common::IsNullOrEmptyString($request['CryptoUnitCode'])
        ) {
            return response()->json([
                'ErrorMessageEN' => ['[CryptoSetPrice] Cannot Equal 0 and [CryptoUnitCode] Cannot Be NULL'],
                'ErrorMessageTH' => ['[ราคาของเหรียญคริปโต]ต้องไม่เท่ากับ 0 และ [หน่วยของเหรียญคริปโต]ต้องไม่เป็นค่าว่าง'],
            ], 400);
        }
        #endregion

        #region Binance Deposit Transaction Verify
        $txId = $request['TxId'];
        $address = $request['Address'];

        $sessionRegTime = env('SESSION_REGISTER_MINUTE_TIME');
        $startTimestamp = Carbon::now(7)->addMinute($sessionRegTime * -1);
        $endTimestamp = Carbon::now(7);
        $getDepositResponse =  $binanceService->getDepositHistory($cryptoCurrency->Symbol, $startTimestamp, $endTimestamp, null);
        if ($getDepositResponse->ok() == false) {
            return response()->json([
                'ErrorMessageEN' => ['Binance Connection Error !'],
                'ErrorMessageTH' => ['เกิดข้อผิดพลาดในการเชื่อมต่อ Binance'],
            ], 400);
        }
        $transactions = collect(json_decode($getDepositResponse, true));
        $verifyResult = $transactions->where('txId', '=', $txId)
            ->where('address', '=', $address)
            ->where('coin', $request["CryptoUnitCode"])
            ->first();

        if ($verifyResult == null) {
            return response()->json([
                'ErrorMessageEN' => ['Verify Result : Deposit Transaction Not Found'],
                'ErrorMessageTH' => ['ผลการตรวจสอบ : ไม่พบการชำระเงิน/ฝากเงิน'],
            ], 403);
        }

        $diffAcceptPercent = env('DIFF_ACCEPT_PERCENT');
        $verifyAmount = $verifyResult['amount'];
        $cryptoSetPric = $request['CryptoSetPrice'];
        $minLimit = $cryptoSetPric * (1 - ($diffAcceptPercent / 100));
        if ($verifyAmount < $minLimit) {
            return response()->json([
                'ErrorMessageEN' => ['Verify Result : Total Deposit is unsuccess'],
                'ErrorMessageTH' => ['ผลการตรวจสอบ : ยอดชำระ/ฝากเงินไม่ครบถ้วน'],
            ], 400);
        }
        #endregion

        #region Internal Payment Used Verify
        $existPayments = Payment::where('TransactionId', $verifyResult['txId'])->limit(1)->get();
        if (count($existPayments) > 0) {
            return response()->json([
                'ErrorMessageEN' => ['Verify Result : Payment Transaction is used'],
                'ErrorMessageTH' => ['ผลการตรวจสอบ : การชำระเงินได้นำไปใช้งานแล้ว'],
            ], 403);
        }
        #endregion

        #region Save
        $paymentData = [
            'orderType' => 'DPS' . $verifyResult['transferType'] . $verifyResult['walletType'],
            'transactionId' => $verifyResult['txId'],
            'transactionTime' => $verifyResult['insertTime'],
            'amount' => $verifyResult['amount'],
            'currency' => $verifyResult['coin'],
        ];
        data_fill($request, 'AccessToken',  $token);
        $service->renewAccount($request, $paymentData, $packagePrice, $cryptoCurrency, $registerService);
        #endregion

        return response()->json([
            'MessageEN' => ['Account renew is successfully'],
            'MessageTH' => ['ต่ออายุบัญชีเสร็จสิ้น'],
        ], 200);
    }
    #endregion


}
