<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;
use App\Constants\Common;
use App\Service\PackagePriceService;
use App\Service\CryptoCurrencyService;
use App\Service\RegisterService;
use App\Service\BinanceProviderService;
use App\Models\Payment;
use Exception;

class RegisterController extends Controller
{
    #region Initial Register
    function save(
        Request $request,
        RegisterService $service,
        PackagePriceService $packagePriceService,
        CryptoCurrencyService $cryptoCurrencyService
    ) {
        $registerData = $request->json('data');

        #region Validate Request
        if (isset($registerData['Login']) === false) {
            data_fill($registerData, 'Login', $registerData['Email']);
        } else {
            $registerData['Login'] = $registerData['Email'];
        }

        if (isset($registerData['Password']) === false) {
            data_fill($registerData, 'Password', '');
        }

        $dummyProfile = uniqid();
        if (isset($registerData['ProfileName']) === false) {
            data_fill($registerData, 'ProfileName', $dummyProfile);
        } else if (Common::IsNullOrEmptyString($registerData['ProfileName'])) {
            $registerData['ProfileName'] = $dummyProfile;
        }

        if (isset($registerData['CryptoSetPrice']) === false) {
            data_fill($registerData, 'CryptoSetPrice', 0);
        }
        if (isset($registerData['CryptoUnitCode']) === false) {
            data_fill($registerData, 'CryptoUnitCode', '');
        }

        $validateResults =  $service->validateMemberAccount($registerData);
        if (count($validateResults) > 0) {
            return response()->json($validateResults, 400);
        }

        if (Cache::store('dupRequest')->has($registerData['Email'])) {
            return response()->json([
                'ErrorMessageEN' => ['Duplicate request, Wait for a few minutes'],
                'ErrorMessageTH' => ['ไม่สามารถร้องขอต่อเนื่องได้'],
            ], 400);
        }

        $packagePrice = $packagePriceService->get($registerData['PackagePriceId']);
        $cryptoCurrency = $cryptoCurrencyService->get($registerData['CryptoCurrencyId']);
        if ($packagePrice == null || $cryptoCurrency == null) {
            return response()->json([
                'ErrorMessageEN' => ['PackagePrice or CryptoCurrency not found.'],
                'ErrorMessageTH' => ['ไม่พบตารางราคาหรือข้อมูลเหรียญคริปโต'],
            ], 400);
        }

        $allowZeroPrice = env('ALLOW_REGISTER_ZERO_PRICE');
        if (
            $allowZeroPrice == false &&
            ($registerData['CryptoSetPrice']  <= 0 || $packagePrice->SetPrice <= 0
                || Common::IsNullOrEmptyString($registerData['CryptoUnitCode'])
            )
        ) {
            return response()->json([
                'ErrorMessageEN' => ['[CryptoSetPrice] Cannot Equal 0 and [CryptoUnitCode] Cannot Be NULL'],
                'ErrorMessageTH' => ['[ราคาของเหรียญคริปโต]ต้องไม่เท่ากับ 0 และ [หน่วยของเหรียญคริปโต]ต้องไม่เป็นค่าว่าง'],
            ], 400);
        }

        if (
            $allowZeroPrice == true &&
            (($registerData['CryptoSetPrice'] == 0 && $packagePrice->SetPrice > 0)
                || Common::IsNullOrEmptyString($registerData['CryptoUnitCode'])
            )
        ) {
            return response()->json([
                'ErrorMessageEN' => ['[CryptoSetPrice] Cannot Equal 0 and [CryptoUnitCode] Cannot Be NULL'],
                'ErrorMessageTH' => ['[ราคาของเหรียญคริปโต]ต้องไม่เท่ากับ 0 และ [หน่วยของเหรียญคริปโต]ต้องไม่เป็นค่าว่าง'],
            ], 400);
        }
        #endregion

        #region Save
        if ($allowZeroPrice === true && $packagePrice->SetPrice == 0) {
            $paymentData['transactionId'] = $registerData['Email'];
            $paymentData['orderType'] = "FREE7DAY";
            $paymentData['transactionTime'] = now(7);
            $paymentData['amount'] = $packagePrice->SetPrice;
            $paymentData['currency'] = $registerData['CryptoUnitCode'];

            $existPayments = Payment::where('TransactionId', $paymentData['transactionId'])->limit(1)->get();
            if (count($existPayments) > 0) {
                return response()->json([
                    'ErrorMessageEN' => ['Verify Result : Privilege of account has been using'],
                    'ErrorMessageTH' => ['ผลการตรวจสอบ : บัญชีนี้มีการใช้สิทธิ์พิเศษแล้ว'],
                ], 403);
            }

            $registerId = $service->registerMember($registerData, $paymentData, $packagePrice, $cryptoCurrency);
            $refId = uniqid();
            $sessionLinkTime = env('SESSION_LINK_MINUTE_TIME');
            $expiredTime =  now(7)->addMinute($sessionLinkTime != null ? $sessionLinkTime : 5);
            cache([$refId => $registerId], $expiredTime);
            return response()
                ->json([
                    "RegisterId" => $refId,
                    "Status" => 20,
                    "StatusText" => "Done"
                ], 200)
                ->header('Content-Type', 'application/json');
        } else {
            $sessionRegTime = env('SESSION_REGISTER_MINUTE_TIME');
            $regId = $service->saveRegisterToCache($registerData, $sessionRegTime);
            return response()
                ->json([
                    "RegisterId" => $regId,
                    "Status" => 10,
                    "StatusText" => "Wait For Verify"
                ], 200)
                ->header('Content-Type', 'application/json');
        }
        #endregion
    }
    #endregion

    #region Internal Chain Payment
    function getpaymentMethod(Request $request, RegisterService $service)
    {
        $regId = $request->json('RegisterId');
        if (Cache::store('register')->has($regId)) {
            $result = $service->getpaymentMethod($request);
            return response()
                ->json($result, 200)
                ->header('Content-Type', 'application/json');
        } else {
            return response()->json([
                'ErrorMessageEN' => ['RegisterId not found'],
                'ErrorMessageTH' => ['ไม่พบเลขที่การลงทะเบียน(RegisterId)'],
            ], 403);
        }
    }

    function cryptoPaymentVerify(
        Request $request,
        RegisterService $service,
        PackagePriceService $packagePriceService,
        CryptoCurrencyService $cryptoCurrencyService,
        BinanceProviderService $binanceService
    ) {
        $regId = $request['RegisterId'];
        if (Cache::store('register')->has($regId)) {

            $registerData = Cache::store('register')->get($regId);

            #region ExistUser Verify
            $validateResults =  $service->validateMemberAccount($registerData);
            if (count($validateResults) > 0) {
                return response()->json($validateResults, 400);
            }
            #endregion

            #region PackagePrice&CryptoCurrency Verify
            $packagePrice = $packagePriceService->get($registerData['PackagePriceId']);
            $cryptoCurrency = $cryptoCurrencyService->get($registerData['CryptoCurrencyId']);
            if ($packagePrice == null || $cryptoCurrency == null) {
                return response()->json([
                    'ErrorMessageEN' => ['PackagePrice or CryptoCurrency not found.'],
                    'ErrorMessageTH' => ['ไม่พบตารางราคาหรือข้อมูลเหรียญคริปโต'],
                ], 400);
            }

            $allowZeroPrice = env('ALLOW_REGISTER_ZERO_PRICE');
            if (
                $allowZeroPrice === false &&
                ($registerData['CryptoSetPrice']  <= 0 || $packagePrice->SetPrice <= 0
                    || Common::IsNullOrEmptyString($registerData['CryptoUnitCode'])
                )
            ) {
                return response()->json([
                    'ErrorMessageEN' => ['[CryptoSetPrice] Cannot Equal 0 and [CryptoUnitCode] Cannot Be NULL'],
                    'ErrorMessageTH' => ['[ราคาของเหรียญคริปโต]ต้องไม่เท่ากับ 0 และ [หน่วยของเหรียญคริปโต]ต้องไม่เป็นค่าว่าง'],
                ], 400);
            }
            #endregion

            #region Binance Payment Transaction Verify
            $prefixTranId = $request['PrefixTransactionId'];
            $suffixTranId = $request['SuffixTransactionId'];

            $sessionRegTime = env('SESSION_REGISTER_MINUTE_TIME');
            $startTimestamp = Carbon::now(7)->addMinute($sessionRegTime * -1);
            $endTimestamp = Carbon::now(7);
            $getPayTranResponse =  $binanceService->getPayTransactions($startTimestamp, $endTimestamp);
            if ($getPayTranResponse->ok() == false) {
                return response()->json([
                    'ErrorMessageEN' => ['Binance Connection Error !'],
                    'ErrorMessageTH' => ['เกิดข้อผิดพลาดในการเชื่อมต่อ Binance'],
                ], 400);
            }
            $transactions = collect($getPayTranResponse["data"]);
            $verifyResult = $transactions->where('amount', '>', 0)
                ->where('currency', $registerData["CryptoUnitCode"])
                ->filter(function ($q) use ($prefixTranId, $suffixTranId) {
                    return Str::startsWith($q['transactionId'], 'C_R_' . $prefixTranId)
                        && Str::endsWith($q['transactionId'],  $suffixTranId);
                })->first();

            if ($verifyResult == null) {
                return response()->json([
                    'ErrorMessageEN' => ['Verify Result : Payment Transaction Not Found'],
                    'ErrorMessageTH' => ['ผลการตรวจสอบ : ไม่พบการชำระเงิน'],
                ], 403);
            }

            $diffAcceptPercent = env('DIFF_ACCEPT_PERCENT');
            $verifyAmount = $verifyResult['amount'];
            $cryptoSetPric = $registerData['CryptoSetPrice'];
            $minLimit = $cryptoSetPric * (1 - ($diffAcceptPercent / 100));
            if ($verifyAmount < $minLimit) {
                return response()->json([
                    'ErrorMessageEN' => ['Verify Result : Total Payment is unsuccess'],
                    'ErrorMessageTH' => ['ผลการตรวจสอบ : ยอดชำระไม่ครบถ้วน'],
                ], 400);
            }
            #endregion

            #region Internal Payment Used Verify
            $existPayments = Payment::where('TransactionId', $verifyResult['transactionId'])->limit(1)->get();
            if (count($existPayments) > 0) {
                return response()->json([
                    'ErrorMessageEN' => ['Verify Result : Payment Transaction is used'],
                    'ErrorMessageTH' => ['ผลการตรวจสอบ : การชำระเงินได้นำไปใช้งานแล้ว'],
                ], 403);
            }
            #endregion

            #region Save
            $registerId = $service->registerMember($registerData, $verifyResult, $packagePrice, $cryptoCurrency);
            $refId = uniqid();
            $sessionLinkTime = env('SESSION_LINK_MINUTE_TIME');
            $expiredTime =  now(7)->addMinute($sessionLinkTime != null ? $sessionLinkTime : 5);
            cache([$refId => $registerId], $expiredTime);
            #endregion
            Cache::store('register')->forget($regId);
            Cache::store('dupRequest')->forget($registerData['Email']);
            return response()
                ->json(["ReferenceId" => $refId], 200)
                ->header('Content-Type', 'application/json');
        } else {
            return response()->json([
                'ErrorMessageEN' => ['Session of register has expired !'],
                'ErrorMessageTH' => ['เซสชั่นการลงทะเบียนหมดอายุ !'],
            ], 403);
        }
    }
    #endregion

    #region External Chain Payment
    function getAllConfig(Request $request, BinanceProviderService $service)
    {
        $coinSymbol = $request->json('CoinSymbol');
        #region Validate Request
        if (isset($coinSymbol) === false || Common::IsNullOrEmptyString($coinSymbol)) {
            return response()->json([
                'ErrorMessageEN' => ['[CoinSymbol] : Coin is required for deposit address.'],
                'ErrorMessageTH' => ['[CoinSymbol] : ต้องระบุเหรียญในการร้องขอข้อมูลการตั้งค่า'],
            ], 400);
        }
        #endregion

        try {
            $resData = $service->getAllConfig($coinSymbol);
            return $resData;
        } catch (Exception $ex) {
            return $this->InjectionErrorMessage($ex);
        }
    }

    function getDepositAddress(Request $request, BinanceProviderService $service)
    {
        $requestData = $request->json('data');
        #region Validate Request
        if (isset($requestData['CoinSymbol']) === false) {
            return response()->json([
                'ErrorMessageEN' => ['[CoinSymbol] : Coin is required for deposit address.'],
                'ErrorMessageTH' => ['[CoinSymbol] : ต้องระบุเหรียญในการร้องขอที่อยู่การรับโอน'],
            ], 400);
        }
        if (isset($requestData['RegisterId']) === false) {
            return response()->json([
                'ErrorMessageEN' => ['[RegisterId] : Reference is required for deposit address.'],
                'ErrorMessageTH' => ['[RegisterId] : ต้องระบุเลขที่อ้างอิงในการร้องขอที่อยู่การรับโอน'],
            ], 400);
        }
        if (isset($requestData['Network']) === false) {
            data_fill($requestData, 'Network', null);
        }

        $regId = $requestData['RegisterId'];
        if (Cache::store('register')->has($regId) === false) {
            return response()->json([
                'ErrorMessageEN' => ['Register not found'],
                'ErrorMessageTH' => ['ไม่พบการลงทะเบียน'],
            ], 403);
        }
        #endregion
        $response = $service->getDepositAddress($requestData['CoinSymbol'], $requestData['Network']);
        if ($response->ok()) {
            $resData = $response->json();
            return response()
                ->json([
                    "CoinSymbol" => $resData["coin"],
                    "Address" => $resData["address"],
                    "Tag" => $resData["tag"],
                    "Url" => $resData["url"],
                ], 200)
                ->header('Content-Type', 'application/json');
        } else {
            return response()->json([
                'ErrorMessageEN' => ['Binance API Error.'],
                'ErrorMessageTH' => ['การเชื่อมต่อ Binance ไม่สมบูรณ์'],
            ], 400);
        }
    }

    function cryptoDepositVerify(
        Request $request,
        RegisterService $service,
        PackagePriceService $packagePriceService,
        CryptoCurrencyService $cryptoCurrencyService,
        BinanceProviderService $binanceService
    ) {
        $regId = $request['RegisterId'];
        if (Cache::store('register')->has($regId)) {

            $registerData = Cache::store('register')->get($regId);

            #region ExistUser Verify
            $validateResults =  $service->validateMemberAccount($registerData);
            if (count($validateResults) > 0) {
                return response()->json($validateResults, 400);
            }
            #endregion

            #region PackagePrice&CryptoCurrency Verify
            $packagePrice = $packagePriceService->get($registerData['PackagePriceId']);
            $cryptoCurrency = $cryptoCurrencyService->get($registerData['CryptoCurrencyId']);
            if ($packagePrice == null || $cryptoCurrency == null) {
                return response()->json([
                    'ErrorMessageEN' => ['PackagePrice or CryptoCurrency not found.'],
                    'ErrorMessageTH' => ['ไม่พบตารางราคาหรือข้อมูลเหรียญคริปโต'],
                ], 400);
            }
            #endregion

            #region Binance Deposit Transaction Verify
            $txId = $request['TxId'];
            $address = $request['Address'];

            $sessionRegTime = env('SESSION_REGISTER_MINUTE_TIME');
            $startTimestamp = Carbon::now(7)->addMinute($sessionRegTime * -1);
            $endTimestamp = Carbon::now(7);
            $getDepositResponse =  $binanceService->getDepositHistory($cryptoCurrency->Symbol, $startTimestamp, $endTimestamp, null);
            if ($getDepositResponse->ok() == false) {
                return response()->json([
                    'ErrorMessageEN' => ['Binance Connection Error !'],
                    'ErrorMessageTH' => ['เกิดข้อผิดพลาดในการเชื่อมต่อ Binance'],
                ], 400);
            }
            $transactions = collect(json_decode($getDepositResponse, true));
            $verifyResult = $transactions->where('txId', '=', $txId)
                ->where('address', '=', $address)
                ->first();

            if ($verifyResult == null) {
                return response()->json([
                    'ErrorMessageEN' => ['Verify Result : Deposit Transaction Not Found'],
                    'ErrorMessageTH' => ['ผลการตรวจสอบ : ไม่พบการชำระเงิน/ฝากเงิน'],
                ], 403);
            }

            $diffAcceptPercent = env('DIFF_ACCEPT_PERCENT');
            $verifyAmount = $verifyResult['amount'];
            $cryptoSetPric = $registerData['CryptoSetPrice'];
            $minLimit = $cryptoSetPric * (1 - ($diffAcceptPercent / 100));
            if ($verifyAmount < $minLimit) {
                return response()->json([
                    'ErrorMessageEN' => ['Verify Result : Total Deposit is unsuccess'],
                    'ErrorMessageTH' => ['ผลการตรวจสอบ : ยอดชำระ/ฝากเงินไม่ครบถ้วน'],
                ], 400);
            }
            #endregion

            #region Internal Payment Used Verify
            $existPayments = Payment::where('TransactionId', $verifyResult['txId'])->limit(1)->get();
            if (count($existPayments) > 0) {
                return response()->json([
                    'ErrorMessageEN' => ['Verify Result : Payment Transaction is used'],
                    'ErrorMessageTH' => ['ผลการตรวจสอบ : การชำระเงินได้นำไปใช้งานแล้ว'],
                ], 403);
            }
            #endregion

            #region Save
            $paymentData = [
                'orderType' => 'DPS' . $verifyResult['transferType'] . $verifyResult['walletType'],
                'transactionId' => $verifyResult['txId'],
                'transactionTime' => $verifyResult['insertTime'],
                'amount' => $verifyResult['amount'],
                'currency' => $verifyResult['coin'],
            ];
            $registerId = $service->registerMember($registerData, $paymentData, $packagePrice, $cryptoCurrency);
            $refId = uniqid();
            $sessionLinkTime = env('SESSION_LINK_MINUTE_TIME');
            $expiredTime =  now(7)->addMinute($sessionLinkTime != null ? $sessionLinkTime : 5);
            cache([$refId => $registerId], $expiredTime);
            #endregion

            Cache::store('register')->forget($regId);
            Cache::store('dupRequest')->forget($registerData['Email']);
            return response()
                ->json(["ReferenceId" => $refId], 200)
                ->header('Content-Type', 'application/json');
        } else {
            return response()->json([
                'ErrorMessageEN' => ['Session of register has expired !'],
                'ErrorMessageTH' => ['เซสชั่นการลงทะเบียนหมดอายุ !'],
            ], 403);
        }
    }
    #endregion

    #region End Register Process
    function getRegisterCache(Request $request, RegisterService $service)
    {
        $referenceId = cache($request->json('ReferenceId'));
        if ($referenceId !== null) {
            $result = $service->get($referenceId);
            return response()
                ->json($result, 200)
                ->header('Content-Type', 'application/json');
        } else {
            return response()->json([
                'ErrorMessageEN' => ['Session of register has expired !'],
                'ErrorMessageTH' => ['เซสชั่นการลงทะเบียนหมดอายุ !'],
            ], 403);
        }
    }

    // Todo Ball : Remove
    function getPayTransactions(Request $request, BinanceProviderService $service)
    {
        $startTimestamp = Carbon::now(7)->addDays(-30);
        $endTimestamp = Carbon::now(7);
        $response = $service->getPayTransactions($startTimestamp, $endTimestamp);
        if ($response->ok()) {
            return response()
                ->json($response["data"], 200)
                ->header('Content-Type', 'application/json');
        } else {
            return response()->json([
                'ErrorMessageEN' => ['Binance API Error.'],
                'ErrorMessageTH' => ['การเชื่อมต่อ Binance ไม่สมบูรณ์'],
            ], 400);
        }
    }

    // Todo Ball : Remove
    function getDepositHistory(Request $request, BinanceProviderService $service)
    {
        $startTimestamp = Carbon::now(7)->addDays(-90);
        $endTimestamp = Carbon::now(7);
        $response = $service->getDepositHistory('BNB', $startTimestamp, $endTimestamp, null);
        if ($response->ok()) {
            return $response->json();
        } else {
            return response()->json([
                'ErrorMessageEN' => ['Binance API Error.'],
                'ErrorMessageTH' => ['การเชื่อมต่อ Binance ไม่สมบูรณ์'],
            ], 400);
        }
    }
    #endregion

}
