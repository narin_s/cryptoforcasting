<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Constants\Common;
use Exception;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    function GetAuthToken(Request $request)
    {
        $result = '';
        $authorization = $request->header('authorization');
        if ($authorization !== null) {
            $tokenArr = explode(" ", $authorization);
            if (
                count($tokenArr) === 2
                && $tokenArr[0] === 'Bearer'
                && Common::IsNullOrEmptyString($tokenArr[1]) == false
            ) {
                $result =  $tokenArr[1];
            }
        }
        return $result;
    }

    function InjectionErrorMessage(Exception $ex)
    {
        $exMsgs = explode('||', $ex->getMessage());
        if (count($exMsgs) == 2) {
            return response()->json([
                'ErrorMessageEN' => explode('&&', $exMsgs[0]),
                'ErrorMessageTH' => explode('&&', $exMsgs[1]),
            ], 400);
        } else {
            return response()->json([
                'ErrorMessageEN' => explode('&&', $ex->getMessage()),
                'ErrorMessageTH' => explode('&&', $ex->getMessage()),
            ], 400);
        }
    }
}
