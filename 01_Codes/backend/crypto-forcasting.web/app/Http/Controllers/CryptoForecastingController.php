<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use App\Service\CryptoForecastingService;
use App\Service\BinanceProviderService;
use App\Service\UserService;

class CryptoForecastingController extends Controller
{
    function findCurrent(
        Request $request,
        CryptoForecastingService $service,
        BinanceProviderService $binanceService,
        UserService $userService
    ) {
        #region Verifier
        $token = $this->GetAuthToken($request);
        if (Cache::missing($token)) {
            return response(null, 401);
        }

        $loginProfile = cache($token);
        $user = $userService->getUserProfile($loginProfile['PrivateKey']);
        if ($user == null) {
            return response()->json([
                'ErrorMessageEN' => ['User profile not found.'],
                'ErrorMessageTH' => ['ไม่พบโปรไฟล์ผู้ใช้งาน'],
            ], 400);
        }

        $currentDate = now(7);
        if ($user->ExpiredDate < $currentDate) {
            return response()->json([
                'ErrorMessageEN' => ['Access denied. Please Renew Your Package.'],
                'ErrorMessageTH' => ['การเข้าถึงถูกปฏิเสธ กรุณาต่ออายุแพคเกจของคุณ'],
            ], 403);
        }
        #endregion

        $criteria = $request->json('criteria');
        $pageIndex = isset($criteria['PageIndex']) ? $criteria['PageIndex'] : 0;
        $pageSize = isset($criteria['PageSize']) ? $criteria['PageSize'] : 20;
        $symbol = isset($criteria['Symbol']) ? $criteria['Symbol'] : '';
        $results = $service->findCurrent($symbol, $pageIndex, $pageSize, $binanceService);
        return response()
            ->json($results, 200)
            ->header('Content-Type', 'application/json');
    }
}
