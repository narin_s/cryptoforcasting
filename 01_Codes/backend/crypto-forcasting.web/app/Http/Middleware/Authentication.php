<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Constants\Common;

class Authentication
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $authorization = $request->header('authorization');
        if ($authorization === null) {
            return response(null, 403);
        }
        $tokenArr = explode(" ", $authorization);
        if (
            count($tokenArr) !== 2 ||
            (count($tokenArr) === 2 && ($tokenArr[0] !== 'Bearer' || Common::IsNullOrEmptyString($tokenArr[1])))
        ) {
            return response(null, 400);
        }
        $token = $tokenArr[1];
        $loginProfile = cache($token);
        if ($loginProfile !== null) {
            return $next($request);
        } else {
            return response(null, 401);
        }
    }
}
