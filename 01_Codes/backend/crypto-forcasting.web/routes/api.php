<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CryptoCurrencyController;
use App\Http\Controllers\PackagePriceController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\HomeMemberController;
use App\Http\Controllers\AuthenticationController;
use App\Http\Controllers\CryptoForecastingController;
use App\Http\Controllers\UserProfileController;

Route::get('GetToken', function (Request $request) {
    $accessId = $request->header("accessID");
    if ($accessId === 'b9e0cb83d5f9445ca59fcb3a9afffdd8') {
        return response()->json([
            "accessID" => $accessId,
            "sessionToken" => csrf_token(),
        ], 200);
    }
    return abort(403);
});

Route::get('/TestJson', function (Request $request) {
    $sessionID = $request->header("X-CSRF-TOKEN");
    $refId = uniqid();
    cache([$refId => 3], 5);
    return response()->json(
        [
            "AppName" => "Crypto-Forcasting",
            "SessionID" => $sessionID,
            "RefId" => $refId,
        ],
        200,
        [
            'Content-Type' => 'application/json',
            'Charset' => 'utf-8'
        ],
    );
});

Route::post('/CryptoCurrency/GetAll', [CryptoCurrencyController::class, 'getall'])->name('cryptoCurrency');
Route::post('/PackagePrice/GetAll', [PackagePriceController::class, 'getall'])->name('packagePrice');
Route::post('/PackagePrice/GetForRenew', [PackagePriceController::class, 'getForRenew'])->middleware('authCheck');

Route::post('/Register/Save', [RegisterController::class, 'save']);
Route::post('/Register/GetPaymentMethod', [RegisterController::class, 'getpaymentMethod']);
Route::post('/Register/CryptoPaymentVerify', [RegisterController::class, 'cryptoPaymentVerify']);
Route::post('/Register/GetApplicated', [RegisterController::class, 'getRegisterCache']);
Route::post('/Register/GetPayTransactions', [RegisterController::class, 'getPayTransactions']);  // Todo Ball : Remove
Route::post('/Register/GetDepositHistorys', [RegisterController::class, 'getDepositHistory']);  // Todo Ball : Remove
Route::post('/Register/GetAllConfig', [RegisterController::class, 'getAllConfig']); 
Route::post('/Register/GetDepositAddress', [RegisterController::class, 'getDepositAddress']); 
Route::post('/Register/CryptoDepositVerify', [RegisterController::class, 'cryptoDepositVerify']); 


Route::post('/Authen/Login', [AuthenticationController::class, 'login']);
Route::post('/Authen/ForgotPassword', [AuthenticationController::class, 'forgotPassword']);
Route::post('/Authen/ResetPassword', [AuthenticationController::class, 'resetPassword']);
Route::post('/Authen/GetLoginProfile', [AuthenticationController::class, 'getLoginProfile'])->middleware('authCheck');
Route::post('/Authen/Logout', [AuthenticationController::class, 'logout'])->middleware('authCheck');

Route::post('/UserProfile/Get', [UserProfileController::class, 'getUserProfile'])->middleware('authCheck');
Route::post('/UserProfile/RequestOTP', [UserProfileController::class, 'requestOTP'])->middleware('authCheck');
Route::post('/UserProfile/Save', [UserProfileController::class, 'saveUserProfile'])->middleware('authCheck');
Route::post('/UserProfile/GetPaymentMethod', [UserProfileController::class, 'getpaymentMethod'])->middleware('authCheck');
Route::post('/UserProfile/RenewInternalChain', [UserProfileController::class, 'renewAccountInternalChain'])->middleware('authCheck');
Route::post('/UserProfile/GetAllConfig', [UserProfileController::class, 'getAllConfig'])->middleware('authCheck');
Route::post('/UserProfile/GetDepositAddress', [UserProfileController::class, 'getDepositAddress'])->middleware('authCheck');
Route::post('/UserProfile/RenewExternalChain', [UserProfileController::class, 'renewAccountExternalChain'])->middleware('authCheck');

Route::post('/CryptoForecasting/Find', [CryptoForecastingController::class, 'findCurrent'])->middleware('authCheck');

Route::get('/HomeMember', [HomeMemberController::class, 'index'])->name('homeMember')->middleware('authCheck');
