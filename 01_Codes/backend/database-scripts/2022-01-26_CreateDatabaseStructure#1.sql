-- CryptoForcasting_dev.CryptoCurrency definition

CREATE TABLE `CryptoCurrency` (
  `Id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `Symbol` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `UpdatedDate` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `SymbolUnitConvert` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- CryptoForcasting_dev.PackagePrice definition

CREATE TABLE `PackagePrice` (
  `Id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `SeqNo` int unsigned NOT NULL,
  `NameTH` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `NameEN` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Peroid` int unsigned NOT NULL,
  `UnitPeroid` tinyint unsigned NOT NULL,
  `SetPrice` decimal(18,6) NOT NULL,
  `UnitPrice` tinyint unsigned NOT NULL,
  `IsPublish` tinyint(1) NOT NULL DEFAULT '0',
  `CreatedDate` datetime DEFAULT NULL,
  `UpdatedDate` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- CryptoForcasting_dev.Payment definition

CREATE TABLE `Payment` (
  `Id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `Type` tinyint unsigned NOT NULL,
  `ReferenceId` bigint unsigned NOT NULL,
  `SetPriceSnapShot` decimal(18,6) NOT NULL,
  `UnitPriceSnapShot` tinyint unsigned NOT NULL,
  `SymbolSnapShot` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ExchangeRate` decimal(24,14) NOT NULL,
  `TotalPaid` decimal(24,14) NOT NULL,
  `OrderType` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `TransactionId` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `TransactionDate` varchar(26) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ActualPaid` decimal(24,14) NOT NULL,
  `Currency` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `CreatedDate` timestamp NULL DEFAULT NULL,
  `UpdatedDate` timestamp NULL DEFAULT NULL,
  `DeletedDate` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- CryptoForcasting_dev.`User` definition

CREATE TABLE `User` (
  `Id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `ProfileName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Email` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Login` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Password` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `PrivateKey` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `IsActive` tinyint(1) NOT NULL DEFAULT '0',
  `ExpiredDate` datetime DEFAULT NULL,
  `CreatedDate` timestamp NULL DEFAULT NULL,
  `UpdatedDate` timestamp NULL DEFAULT NULL,
  `DeletedDate` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- CryptoForcasting_dev.migrations definition

CREATE TABLE `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*
-- CryptoForcasting_dev.personal_access_tokens definition

CREATE TABLE `personal_access_tokens` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
*/

-- CryptoForcasting_dev.CryptoForecast definition

CREATE TABLE `CryptoForecast` (
  `Id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `CryptoId` bigint unsigned DEFAULT NULL,
  `SymbolSnapShot` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `IsPublish` tinyint(1) NOT NULL DEFAULT '0',
  `CreatedDate` datetime DEFAULT NULL,
  `UpdatedDate` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `SymbolUnitConvSnapShot` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_CryptoId_CryptoCurrency_Id` (`CryptoId`),
  CONSTRAINT `FK_CryptoId_CryptoCurrency_Id` FOREIGN KEY (`CryptoId`) REFERENCES `CryptoCurrency` (`Id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- CryptoForcasting_dev.CryptoForecastItem definition

CREATE TABLE `CryptoForecastItem` (
  `Id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `CryptoForecastId` bigint unsigned NOT NULL,
  `Type` tinyint unsigned NOT NULL,
  `SuggestStatus` tinyint NOT NULL,
  `ForcastDate` date NOT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `UpdatedDate` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_CryptoForecastId_CryptoForecast_Id` (`CryptoForecastId`),
  CONSTRAINT `FK_CryptoForecastId_CryptoForecast_Id` FOREIGN KEY (`CryptoForecastId`) REFERENCES `CryptoForecast` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- CryptoForcasting_dev.Register definition

CREATE TABLE `Register` (
  `Id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `RegisterNo` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ProfileName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `UserId` bigint unsigned NOT NULL,
  `Login` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `SecretKey` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Email` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Status` tinyint unsigned NOT NULL,
  `PeroidSnapShot` int unsigned NOT NULL,
  `UnitPeroidSnapShot` tinyint unsigned NOT NULL,
  `CreatedDate` timestamp NULL DEFAULT NULL,
  `UpdatedDate` timestamp NULL DEFAULT NULL,
  `DeletedDate` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_UserId_User_Id` (`UserId`),
  CONSTRAINT `FK_UserId_User_Id` FOREIGN KEY (`UserId`) REFERENCES `User` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;