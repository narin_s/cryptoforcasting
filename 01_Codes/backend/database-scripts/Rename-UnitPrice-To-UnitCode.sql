ALTER TABLE PackagePrice CHANGE UnitPrice UnitCode tinyint unsigned NOT NULL;
ALTER TABLE Payment CHANGE UnitPriceSnapShot UnitCodeSnapShot tinyint unsigned NOT NULL;
