
CREATE TABLE `CryptoForecastItem`
(
	`Id` BIGINT UNSIGNED NOT NULL,
	`CryptoForecastId` BIGINT UNSIGNED NOT NULL,
	`Type` TINYINT UNSIGNED NOT NULL,
	`SuggestStatus` TINYINT NOT NULL,
	`ForcastDate` DATE NOT NULL,
	`CreatedDate` DATETIME(0),
	`UpdatedDate` DATETIME(0),
	CONSTRAINT `PK_CryptoForecastItem` PRIMARY KEY (`Id`)
)
;

CREATE TABLE `CryptoForecast`
(
	`Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
	`CryptoId` BIGINT UNSIGNED NOT NULL,
	`IsPublish` BOOL NOT NULL DEFAULT false,
	`CreatedTime` DATETIME(0),
	`UpdatedTime` DATETIME(0),
	CONSTRAINT `PK_CryptoForecast` PRIMARY KEY (`Id`)
)
;

CREATE TABLE `PackagePrice`
(
	`Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
	`SeqNo` INTEGER UNSIGNED NOT NULL,
	`NameTH` NVARCHAR(500) NOT NULL,
	`NameEN` VARCHAR(500),
	`Peroid` INT UNSIGNED NOT NULL,
	`UnitPeroid` TINYINT UNSIGNED NOT NULL,
	`SetPrice` DECIMAL(18,8) NOT NULL,
	`UnitPrice` TINYINT UNSIGNED NOT NULL,
	`IsPublish` BOOL NOT NULL DEFAULT false,
	`CreatedDate` DATETIME(0),
	`UpdatedDate` DATETIME(0),
	CONSTRAINT `PK_PackagePrice` PRIMARY KEY (`Id`)
)
;

CREATE TABLE `CryptoCurrency`
(
	`Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
	`Symbol` VARCHAR(50) NOT NULL,
	`Name` VARCHAR(255) NOT NULL,
	`Description` VARCHAR(500),
	`CreatedDate` DATETIME(0),
	`UpdatedDate` DATETIME(0),
	CONSTRAINT `PK_CryptoCurrency` PRIMARY KEY (`Id`)
)
;

CREATE TABLE `User`
(
	`Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT ,
	`ProfileName` NVARCHAR(255) NOT NULL,
	`Email` VARCHAR(255) NOT NULL,
	`Login` NVARCHAR(100) NOT NULL,
	`Password` NVARCHAR(255) NOT NULL,
	`PrivateKey` VARCHAR(100) NOT NULL,
	`IsActive` BOOL NOT NULL DEFAULT false,
	`ExpiredDate` DATETIME(0),
	`CreatedDate` DATETIME(0),
	`UpdatedDate` DATETIME(0),
	CONSTRAINT `PK_User` PRIMARY KEY (`Id`)
)
;

ALTER TABLE `CryptoForecastItem` 
 ADD INDEX `IXFK_CryptoForecastItem_CryptoForecast` (`CryptoForecastId` ASC)
;

ALTER TABLE `CryptoForecast` 
 ADD INDEX `IXFK_CryptoForecast_CryptoCurrency` (`CryptoId` ASC)
;

ALTER TABLE `CryptoForecastItem` 
 ADD CONSTRAINT `FK_CryptoForecastItem_CryptoForecast`
	FOREIGN KEY (`CryptoForecastId`) REFERENCES `CryptoForecast` (`Id`) ON DELETE Restrict ON UPDATE Restrict
;

ALTER TABLE `CryptoForecast` 
 ADD CONSTRAINT `FK_CryptoForecast_CryptoCurrency`
	FOREIGN KEY (`CryptoId`) REFERENCES `CryptoCurrency` (`Id`) ON DELETE Restrict ON UPDATE Restrict
;
