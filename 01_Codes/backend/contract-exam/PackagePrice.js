 const data = [
        {
          "CoinName":"THB",
          "Pricing":[
            {
              "Id": 1,
              "SeqNo": 1,
              "NameTH": "1 สัปดาห์",
              "NameEN": "A Week",
              "Peroid": 7,
              "UnitPeroid": 10,
              "IsPublish": 1,
              "SetPrice": "1500.50000000",
              "UnitPrice": 50
            },
            {
              "Id": 2,
              "SeqNo": 2,
              "NameTH": "2 สัปดาห์",
              "NameEN": "Two Week",
              "Peroid": 14,
              "UnitPeroid": 10,
              "IsPublish": 1,
              "SetPrice": "2800.75000000",
              "UnitPrice": 50
            },
          ]
        },
        {
          "CoinName":"IOST",
          "Pricing":[
            {
              "Id": 1,
              "SeqNo": 1,
              "NameTH": "1 สัปดาห์",
              "NameEN": "A Week",
              "Peroid": 7,
              "UnitPeroid": 10,
              "IsPublish": 1,
              "SetPrice": "1500.50000000",
              "UnitPrice": 50
            },
            {
              "Id": 2,
              "SeqNo": 2,
              "NameTH": "2 สัปดาห์",
              "NameEN": "Two Week",
              "Peroid": 14,
              "UnitPeroid": 10,
              "IsPublish": 1,
              "SetPrice": "2800.75000000",
              "UnitPrice": 50
            },
          ]
        },
        {
          "CoinName":"LTC",
          "Pricing":[
            {
              "Id": 1,
              "SeqNo": 1,
              "NameTH": "1 สัปดาห์",
              "NameEN": "A Week",
              "Peroid": 7,
              "UnitPeroid": 10,
              "IsPublish": 1,
              "SetPrice": "1500.50000000",
              "UnitPrice": 50
            },
            {
              "Id": 2,
              "SeqNo": 2,
              "NameTH": "2 สัปดาห์",
              "NameEN": "Two Week",
              "Peroid": 14,
              "UnitPeroid": 10,
              "IsPublish": 1,
              "SetPrice": "2800.75000000",
              "UnitPrice": 50
            },
          ]
        },
        {
          "CoinName":"DOGE",
          "Pricing":[
            {
              "Id": 1,
              "SeqNo": 1,
              "NameTH": "1 สัปดาห์",
              "NameEN": "A Week",
              "Peroid": 7,
              "UnitPeroid": 10,
              "IsPublish": 1,
              "SetPrice": "1500.50000000",
              "UnitPrice": 50
            },
            {
              "Id": 2,
              "SeqNo": 2,
              "NameTH": "2 สัปดาห์",
              "NameEN": "Two Week",
              "Peroid": 14,
              "UnitPeroid": 10,
              "IsPublish": 1,
              "SetPrice": "2800.75000000",
              "UnitPrice": 50
            },
          ]
        },
        {
          "CoinName":"BNB",
          "Pricing":[
            {
              "Id": 1,
              "SeqNo": 1,
              "NameTH": "1 สัปดาห์",
              "NameEN": "A Week",
              "Peroid": 7,
              "UnitPeroid": 10,
              "IsPublish": 1,
              "SetPrice": "1500.50000000",
              "UnitPrice": 50
            },
            {
              "Id": 2,
              "SeqNo": 2,
              "NameTH": "2 สัปดาห์",
              "NameEN": "Two Week",
              "Peroid": 14,
              "UnitPeroid": 10,
              "IsPublish": 1,
              "SetPrice": "2800.75000000",
              "UnitPrice": 50
            },
          ]
        }
      ]