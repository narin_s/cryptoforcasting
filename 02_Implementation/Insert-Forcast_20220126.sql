--  Auto-generated SQL script #202202081409
INSERT INTO CryptoForecast (CryptoId,SymbolSnapShot,IsPublish,CreatedDate,SymbolUnitConvSnapShot)
	VALUES (1,'BTC',1,'2022-02-01','BTCUSDT');
INSERT INTO CryptoForecast (CryptoId,SymbolSnapShot,IsPublish,CreatedDate,SymbolUnitConvSnapShot)
	VALUES (6,'ETH',1,'2022-02-01','ETHUSDT');
INSERT INTO CryptoForecast (CryptoId,SymbolSnapShot,IsPublish,CreatedDate,SymbolUnitConvSnapShot)
	VALUES (7,'ADA',1,'2022-02-01','ADAUSDT');
INSERT INTO CryptoForecast (CryptoId,SymbolSnapShot,IsPublish,CreatedDate,SymbolUnitConvSnapShot)
	VALUES (8,'XRP',1,'2022-02-01','XRPUSDT');
INSERT INTO CryptoForecast (CryptoId,SymbolSnapShot,IsPublish,CreatedDate,SymbolUnitConvSnapShot)
	VALUES (9,'SHIB',1,'2022-02-01','SHIBUSDT');
INSERT INTO CryptoForecast (CryptoId,SymbolSnapShot,IsPublish,CreatedDate,SymbolUnitConvSnapShot)
	VALUES (10,'AVAX',1,'2022-02-01','AVAXUSDT');
INSERT INTO CryptoForecast (CryptoId,SymbolSnapShot,IsPublish,CreatedDate,SymbolUnitConvSnapShot)
	VALUES (11,'LINK',1,'2022-02-01','LINKUSDT');
INSERT INTO CryptoForecast (CryptoId,SymbolSnapShot,IsPublish,CreatedDate,SymbolUnitConvSnapShot)
	VALUES (12,'XLM',1,'2022-02-01','XLMUSDT');
INSERT INTO CryptoForecast (CryptoId,SymbolSnapShot,IsPublish,CreatedDate,SymbolUnitConvSnapShot)
	VALUES (13,'NEAR',1,'2022-02-01','NEARUSDT');
INSERT INTO CryptoForecast (CryptoId,SymbolSnapShot,IsPublish,CreatedDate,SymbolUnitConvSnapShot)
	VALUES (14,'AXS',1,'2022-02-01','AXSUSDT');
INSERT INTO CryptoForecast (CryptoId,SymbolSnapShot,IsPublish,CreatedDate,SymbolUnitConvSnapShot)
	VALUES (15,'MATIC',1,'2022-02-01','MATICUSDT');
INSERT INTO CryptoForecast (CryptoId,SymbolSnapShot,IsPublish,CreatedDate,SymbolUnitConvSnapShot)
	VALUES (16,'THETA',1,'2022-02-01','THETAUSDT');
INSERT INTO CryptoForecast (CryptoId,SymbolSnapShot,IsPublish,CreatedDate,SymbolUnitConvSnapShot)
	VALUES (17,'CRV',1,'2022-02-01','CRVUSDT');
INSERT INTO CryptoForecast (CryptoId,SymbolSnapShot,IsPublish,CreatedDate,SymbolUnitConvSnapShot)
	VALUES (18,'LUNA',1,'2022-02-01','LUNAUSDT');
